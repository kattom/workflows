

#  Resources - GRCh38


* [Center core never more](https://www.youtube.com/watch?v=rXUb4gdgpbI&feature=youtu.be)


# Table of Contents

<!---toc start-->
* [Resources](#resources)
  * [Reference](#reference)
    * [Broad institute hg38](#broad-institute-hg39)
  * [Intervals](#intervals)
    * [Agilent](#agilent)
    * [UCSC genes](#ucsc-genes)
  * [Frequencies](#frequencies)
    * [All frequencies - exome](#all-frequencies---exome)
    * [All frequencies - genome](#all-frequencies---genome)
    * [1000 Genomes](#1000-genomes)
    * [ESP6500](#esp6500)
    * [ExAC](#exac)
    * [gnomAD exomes](#gnomad-exomes)
    * [gnomAD genomes](#gnomad-genomes)
    * [gnomAD genomes - exome calling intervals](#gnomad-genomes---exome-calling-intervals)
    * [Mitomap](#mitomap)
   * [Functional annotations - variant level](#functional-annotations---variant-level)
     * [dbSNP](#dbsnp)
     * [ClinVar](#clinvar)
     * [Mitomap - diseases](#mitomap---diseases)
   * [Functional annotations - gene level](#functional-annotations---gene-level)
     * [ClinVar - diseases](#clinvar---diseases)
     * [Human Phenotype Ontology](#human-phenotype-ontology)
    * [Scores](#scores)
      * [CADD](#cadd)
      * [GERP](#gerp)
      * [M-CAP](#m-cap)
      * [PhastCons](#phastcons)
      * [PhyloP](#phylop)
      * [SIFT](#sift)



<!---toc end-->

# Resources

# Reference

## Broad institute hg38

**Last update date:** 29-01-2019
**Update requirements:** almost never


**1.**  Download hg38 reference genome files from [Broad FTP server](https://software.broadinstitute.org/gatk/download/bundle):

```
wget -c ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/hg38/Homo_sapiens_assembly38.fasta.gz
wget -c ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/hg38/Homo_sapiens_assembly38.fasta.fai
wget -c ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/hg38/Homo_sapiens_assembly38.dict
wget -c ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/hg38/Homo_sapiens_assembly38.fasta.64.alt

gunzip Homo_sapiens_assembly38.fasta.gz

mv Homo_sapiens_assembly38.fasta Homo_sapiens_assembly38.fa
mv Homo_sapiens_assembly38.fasta.fai Homo_sapiens_assembly38.fa.fai
mv Homo_sapiens_assembly38.fasta.64.alt Homo_sapiens_assembly38.fa.alt
```
**2.** Create bwa and samtools index files

**Requires:** bwa
**Requires:** samtools

```
bwa index -a bwtsw Homo_sapiens_assembly38.fa
samtools faidx Homo_sapiens_assembly38.fa
```

# Intervals

## Agilent

**Last update date:** 18-02-2019
**Update requirements:** when new kit is released



 **1.** Login to [Agilent SureDesign](https://earray.chem.agilent.com/suredesign/).  Naviagte to "Find Designs" --> "SureSelect DNA" -->"Agilent Catalog" . Download files for selected kits - select hg38 genome.

 - **SureSelect Human All Exon V7** (S31285117) (hg38)
 - **SureSelect Clinical Research Exome V2** (S30409818) (hg38)
 - **SureSelect Human All Exon V6+UTR r2** (S07604624) (hg38)
 - **SureSelect Human All Exon V6 r2** (S07604514) (hg38)
 - **SureSelect Human All Exon V6+COSMIC** r2 (S07604715) (hg38)

**2.** Extract files:
```
## SureSelect Human All Exon V7
mkdir sureselect-human-all-exon-v7
mv S31285117_hs_hg38.zip sureselect-human-all-exon-v7; cd sureselect-human-all-exon-v7
unzip S31285117_hs_hg38.zip; rm S31285117_hs_hg38.zip; cd ../

## SureSelect Clinical Research Exome V2
mkdir sureselect-clinical-research-exome-v2
mv S30409818_hs_hg38.zip; cd sureselect-clinical-research-exome-v2
unzip S30409818_hs_hg38.zip; rm S30409818_hs_hg38.zip; cd ../

## SureSelect Human All Exon V6+UTR r2
mkdir sureselect-human-all-exon-v6-utr-r2
mv S07604624_hs_hg38.zip sureselect-human-all-exon-v6-utr-r2; cd sureselect-human-all-exon-v6-utr-r2
unzip S07604624_hs_hg38.zip; rm S07604624_hs_hg38.zip; cd ../

## SureSelect Human All Exon V6 r2
mkdir sureselect-human-all-exon-v6-r2
mv S07604514_hs_hg38.zip sureselect-human-all-exon-v6-r2; cd sureselect-human-all-exon-v6-r2
unzip S07604514_hs_hg38.zip; rm S07604514_hs_hg38.zip; cd ../

## SureSelect Human All Exon V6+COSMIC r2
mkdir sureselect-human-all-exon-v6-cosmic-r
mv S07604715_hs_hg38.zip sureselect-human-all-exon-v6-cosmic-r2; cd sureselect-human-all-exon-v6-cosmic-r2
unzip S07604715_hs_hg38.zip; rm S07604715_hs_hg38.zip; cd ../
```

**3.** Prepare GRCh38 dictionary and genome file for bedtools slop:

-   **Homo_sapiens_assembly38.dict** - reference genome, from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

```
cat Homo_sapiens_assembly38.dict | grep "^@SQ" | cut -f 2,3 | grep -v "HLA" | sed "s/SN://g" | sed "s/LN://g" > Homo_sapiens_assembly38.genome
```

**4.** Create BED files containing intervals from *_Covered file extended 200 bp at each side:

**Required:**  bedtools

```
## SureSelect Human All Exon V7
bedtools slop -b 200 -i sureselect-human-all-exon-v7/S31285117_Covered.bed -g Homo_sapiens_assembly38.genome > sureselect-human-all-exon-v7/sureselect-human-all-exon-v7.covered-padded-200.bed

## SureSelect Clinical Research Exome V2
bedtools slop -b 200 -i sureselect-clinical-research-exome-v2/S30409818_Covered.bed -g Homo_sapiens_assembly38.genome > sureselect-clinical-research-exome-v2/sureselect-clinical-research-exome-v2.covered-padded-200.bed

## SureSelect Human All Exon V6+UTR r2
bedtools slop -b 200 -i sureselect-human-all-exon-v6-utr-r2/S07604624_Covered.bed -g Homo_sapiens_assembly38.genome > sureselect-human-all-exon-v6-utr-r2/sureselect-human-all-exon-v6-utr-r2.covered-padded-200.bed

## SureSelect Human All Exon V6 r2
bedtools slop -b 200 -i sureselect-human-all-exon-v6-r2/S07604514_Covered.bed -g Homo_sapiens_assembly38.genome > sureselect-human-all-exon-v6-r2/sureselect-human-all-exon-v6-r2.covered-padded-200.bed

## SureSelect Human All Exon V6+COSMIC r2
bedtools slop -b 200 -i sureselect-human-all-exon-v6-cosmic-r2/S07604715_Covered.bed -g Homo_sapiens_assembly38.genome > sureselect-human-all-exon-v6-cosmic-r2/sureselect-human-all-exon-v6-cosmic-r2.covered-padded-200.bed
```

**4.** Create Picard interval_files using  *covered-padded-200.bed files:

**Required:**  Picard

```
## SureSelect Human All Exon V7
java -jar picard.jar BedToIntervalList SD=Homo_sapiens_assembly38.dict I=sureselect-human-all-exon-v7/sureselect-human-all-exon-v7.covered-padded-200.bed O=sureselect-human-all-exon-v7/sureselect-human-all-exon-v7.covered-padded-200.interval_list

## SureSelect Clinical Research Exome V2
java -jar picard.jar BedToIntervalList SD=Homo_sapiens_assembly38.dict I=sureselect-clinical-research-exome-v2/sureselect-clinical-research-exome-v2.covered-padded-200.bed O=sureselect-clinical-research-exome-v2/sureselect-clinical-research-exome-v2.covered-padded-200.interval_list

## SureSelect Human All Exon V6+UTR r2
java -jar picard.jar BedToIntervalList SD=Homo_sapiens_assembly38.dict I=sureselect-human-all-exon-v6-utr-r2/sureselect-human-all-exon-v6-utr-r2.covered-padded-200.bed O=sureselect-human-all-exon-v6-utr-r2/sureselect-human-all-exon-v6-utr-r2.covered-padded-200.interval_list

## SureSelect Human All Exon V6 r2
java -jar picard.jar BedToIntervalList SD=Homo_sapiens_assembly38.dict I=sureselect-human-all-exon-v6-r2/sureselect-human-all-exon-v6-r2.covered-padded-200.bed O=sureselect-human-all-exon-v6-r2/sureselect-human-all-exon-v6-r2.covered-padded-200.interval_list


## SureSelect Human All Exon V6+COSMIC r2
java -jar picard.jar BedToIntervalList SD=Homo_sapiens_assembly38.dict I=sureselect-human-all-exon-v6-cosmic-r2/sureselect-human-all-exon-v6-cosmic-r2.covered-padded-200.bed O=sureselect-human-all-exon-v6-cosmic-r2/sureselect-human-all-exon-v6-cosmic-r2.covered-padded-200.interval_list
```


## BROAD


**Last update date:** 19-02-2019
**Update requirements:** once per few months

**1.** Download **resources_broad_hg38_v0_wgs_calling_regions.hg38.interval_list** from [BROAD resources FTP or Google Cloud](https://software.broadinstitute.org/gatk/download/bundle/)

```
mkdir broad-wgs-calling-regions
mv resources_broad_hg38_v0_wgs_calling_regions.hg38.interval_list /broad-wgs-calling-regions/broad-wgs-calling-regions.inerval_list
```


## UCSC genes


**Last update date:** 19-02-2019
**Update requirements:** once per few months

**1.** Download **refGene.txt.gz** from [UCSC hg38 site with databases](http://hgdownload.cse.ucsc.edu/goldenPath/hg38/database/):

```
wget -c http://hgdownload.cse.ucsc.edu/goldenPath/hg38/database/refGene.txt.gz
```

**2.** Prepare GRCh38 dictionary and genome file for bedtools slop:

-   **Homo_sapiens_assembly38.dict** - reference genome, from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

```
cat Homo_sapiens_assembly38.dict | grep "^@SQ" | cut -f 2,3 | grep -v "HLA" | sed "s/SN://g" | sed "s/LN://g" > Homo_sapiens_assembly38.genome
```

**3.** Extract genes coordinates from refGene.txt.gz, merge them and extend resulting intervals 200bp at each side. Remove intervals from *_alt and *_fix chromosomes:

**Required:**  bedtools

```
zcat refGene.txt.gz | cut -f 3,5,6,13 | sort -t $'\t' -k1,1 -k2,2n | grep -v "_fix" | grep -v "_alt" | bedtools merge -c 4 -o distinct -i - | bedtools slop -b 200 -g Homo_sapiens_assembly38.genome -i - > ucsc-genes-padded-200.bed
```


**4.** Create Picard interval_files using  **ucsc-genes-padded-200.bed** file:

**Required:**  Picard

```
java -jar picard.jar BedToIntervalList SD=Homo_sapiens_assembly38.dict I=ucsc-genes-padded-200.bed O=ucsc-genes-padded-200.interval_list
```


# Frequencies

## All frequencies - exome

**Last update date:** 22-01-2019
**Update requirements:** almost never

**1.** Create chromosome-wise VCF annotation files containing all frequencies annotation for WES:

 - [1000 Genomes](#1000-genomes)
 - [ESC6500](#esc6500)
 - [ExAC](#exac)
 - [gnomAD exomes](#gnomad-exomes)
 - [gnomAD genomes - exome calling intervals](#gnomad-genomes---exome-calling-intervals)
 - [Mitomap](#mitomap)

```
MITOMAP=../mitomap/chrY-and-the-rest.mitomap.vcf.gz

for i in {1..22}; do

	GNOMAD_EXOMES=../gnomad-exomes/chr"$i".gnomad-exomes.vcf.gz
	GNOMAD_GENOMES_EXOME_CALLING_INTERVALS=../gnomad-genomes-exome-calling-intervals/chr"$i".gnomad-genomes-exome-calling-intervals.vcf.gz
	EXAC=../exac/chr"$i".exac.vcf.gz;
	KG=../kg/chr"$i".kg.vcf.gz
	ESP6500=../esp6500/chr"$i".esp6500.vcf.gz

	echo "vcf-merge $GNOMAD_EXOMES $GNOMAD_GENOMES_EXOME_CALLING_INTERVALS $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | bgzip > chr$i.frequencies.vcf.gz &"

done

i="X"
GNOMAD_EXOMES=../gnomad-exomes/chr"$i".gnomad-exomes.vcf.gz
GNOMAD_GENOMES_EXOME_CALLING_INTERVALS=../gnomad-genomes-exome-calling-intervals/chr"$i".gnomad-genomes-exome-calling-intervals.vcf.gz
EXAC=../exac/chr"$i".exac.vcf.gz
KG=../kg/chr"$i".kg.vcf.gz
ESP6500=../esp6500/chr"$i".esp6500.vcf.gz

echo "vcf-merge $GNOMAD_EXOMES $GNOMAD_GENOMES_EXOME_CALLING_INTERVALS $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | bgzip > chr$i.frequencies.vcf.gz &"

i="Y-and-the-rest"
GNOMAD_EXOMES=../gnomad-exomes/chr"$i".gnomad-exomes.vcf.gz
GNOMAD_GENOMES_EXOME_CALLING_INTERVALS=../gnomad-genomes-exome-calling-intervals/chr"$i".gnomad-genomes-exome-calling-intervals.vcf.gz
EXAC=../exac/chr"$i".exac.vcf.gz
KG=../kg/chr"$i".kg.vcf.gz
ESP6500=../esp6500/chr"$i".esp6500.vcf.gz

echo "vcf-merge $GNOMAD_EXOMES $GNOMAD_GENOMES_EXOME_CALLING_INTERVALS $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | bgzip > chr$i.frequencies.vcf.gz &"

```
```
for i in {1..22}; do echo "tabix -p vcf chr$i.frequencies.vcf.gz &"; done; i="X"; echo "tabix -p vcf chr$i.frequencies.vcf.gz &"; i="Y-and-the-rest"; echo "tabix -p vcf chr$i.frequencies.vcf.gz &"

```

## All frequencies - genome

**Last update date:** 22-01-2019
**Update requirements:** almost never

**1.** Create chromosome-wise VCF annotation files containing all frequencies annotation for WES:

 - [1000 Genomes](#1000-genomes)
 - [ESC6500](#esc6500)
 - [ExAC](#exac)
 - [gnomAD exomes](#gnomad-exomes)
 - [gnomAD genomes](#gnomad-genomes)
 - [Mitomap](#mitomap)

```
MITOMAP=../mitomap/chrY-and-the-rest.mitomap.vcf.gz

for i in {1..22}; do

	GNOMAD_EXOMES=../gnomad-exomes/chr"$i".gnomad-exomes.vcf.gz
	GNOMAD_GENOMES=../gnomad-genomes/chr"$i".gnomad-genomes.vcf.gz
	EXAC=../exac/chr"$i".exac.vcf.gz
	KG=../kg/chr"$i".kg.vcf.gz
	ESP6500=../esp6500/chr"$i".esp6500.vcf.gz

	echo "vcf-merge $GNOMAD_EXOMES $GNOMAD_GENOMES $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | bgzip > chr$i.frequencies.vcf.gz &"

done

i="X"
GNOMAD_EXOMES=../gnomad-exomes/chr"$i".gnomad-exomes.vcf.gz
GNOMAD_GENOMES=../gnomad-genomes/chr"$i".gnomad-genomes.vcf.gz
EXAC=../exac/chr"$i".exac.vcf.gz
KG=../kg/chr"$i".kg.vcf.gz
ESP6500=../esp6500/chr"$i".esp6500.vcf.gz

echo "vcf-merge $GNOMAD_EXOMES $GNOMAD_GENOMES $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | bgzip > chr$i.frequencies.vcf.gz &"

i="Y-and-the-rest"
GNOMAD_EXOMES=../gnomad-exomes/chr"$i".gnomad-exomes.vcf.gz
GNOMAD_GENOMES=../gnomad-genomes/chr"$i".gnomad-genomes.vcf.gz
EXAC=../exac/chr"$i".exac.vcf.gz
KG=../kg/chr"$i".kg.vcf.gz
ESP6500=../esp6500/chr"$i".esp6500.vcf.gz

echo "vcf-merge $GNOMAD_EXOMES $GNOMAD_GENOMES $EXAC $KG $ESP6500 $MITOMAP | python3 ./keep-or-remove-vcf-fields.py -r -l AC,AN,SF | grep -v \"##reference\" | grep -v \"##FILTER\" | grep -v \"##source\" | grep -v \"##contig\" | grep -v -i \"##filedate\" | awk '{if(/^#/) print; else print \$1\"\\t\"\$2\"\\t\"\$3\"\\t\"\$4\"\\t\"\$5\"\\t\"\$6\"\\t.\\t\"\$8}' | bgzip > chr$i.frequencies.vcf.gz &"
```
```
for i in {1..22}; do echo "tabix -p vcf chr$i.frequencies.vcf.gz &"; done; i="X"; echo "tabix -p vcf chr$i.frequencies.vcf.gz &"; i="Y-and-the-rest"; echo "tabix -p vcf chr$i.frequencies.vcf.gz &"

```

## 1000 Genomes

**Last update date:** 04-01-2019
**Update requirements:** almost never

**1.** Download 1000 Genomes database with SNV calls, aligned directly to GRCh38, from [1000 Genomes FTP](http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20181203_biallelic_SNV/). The database is briefly described in this [1000 Genomes blogpost](http://www.internationalgenome.org/announcements/Variant-calls-from-1000-Genomes-Project-data-calling-against-GRCh38/):
```
wget -c http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/1000_genomes_project/release/20181203_biallelic_SNV/ALL.wgs.shapeit2_integrated_v1a.GRCh38.20181129.sites.vcf.gz
```

**2.** Create chromosome-wise VCF annotation files:

**Required**: Biopython

```
zcat ALL.wgs.shapeit2_integrated_v1a.GRCh38.20181129.sites.vcf.gz | python3 ./create-kg-annotation-file.py ./
for i in {1..22}; do tabix -p vcf chr$i.kg.vcf.gz; done
tabix -p vcf chrX.kg.vcf.gz
tabix -p vcf chrY-and-the-rest.kg.vcf.gz
```
**List of 1000 Genomes INFO fields:**

**Source:** 1000 Genomes
**Version:** 1000 Genomes - 2018-12-03 biallelic SNV

| INFO           | Number | Type  | Description                                                                          |
|----------------|--------|-------|--------------------------------------------------------------------------------------|
| ISEQ_KG_AF     | A      | Float | Alternate allele frequency in samples - 1000 Genomes                                 |
| ISEQ_KG_EAS_AF | A      | Float | Alternate allele frequency in samples - 1000 Genomes, East Asian population (EAS)    |
| ISEQ_KG_EUR_AF | A      | Float | Alternate allele frequency in samples - 1000 Genomes, European population (EUR)      |
| ISEQ_KG_ARF_AF | A      | Float | Alternate allele frequency in samples - 1000 Genomes, African population (AFR)       |
| ISEQ_KG_AMR_AF | A      | Float | Alternate allele frequency in samples - 1000 Genomes, Mixed American population (AMR |
| ISEQ_KG_SAS_AF | A      | Float | Alternate allele frequency in samples - 1000 Genomes, South Asian population (SAS)   |


## ESP6500

**Last update date:** 07-01-2019  
**Update requirements:** almost never

**1.** Download ESC6500 database in VCF format, with annotation including positions lifted over to GRCh38,  from  [NHLBI Exome Sequencing Project (ESP) Download page](http://evs.gs.washington.edu/EVS/):

```
wget -c http://evs.gs.washington.edu/evs_bulk_data/ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels.vcf.tar.gz
tar -zxvf ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels.vcf.tar.gz
cat ESP6500SI-V2-SSA137.GRCh38-liftover.chr1.snps_indels.vcf | grep "^#" | grep -v "##[Q|G|K]" > esp6500.hg19.vcf
cat ESP6500SI-V2-SSA137.GRCh38-liftover.chr* | grep -v "^#" >> esp6500.hg19.vcf
rm ESP*
```

**2.**  Download files necessary for liftovering the database from GRCh37/hg19 to hg38:

 * **Homo_sapiens_assembly38.fasta.gz** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

 *  **hg19ToHg38.over.chain.gz** - chain file, from [CrossMap site](http://crossmap.sourceforge.net/)

	```
	wget -c http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz
	```

**3.**  Perform liftovering using CrossMap.py, version 0.3.0:

```
CrossMap.py vcf hg19ToHg38.over.chain.gz  esp6500.hg19.vcf  Homo_sapiens_assembly38.fasta esp6500.hg38.vcf
```
```
@ 2019-01-21 16:01:37: Read chain_file:  hg19ToHg38.over.chain.gz
@ 2019-01-21 16:01:37: Updating contig field ...
@ 2019-01-21 16:02:29: Total entries: 1986331
@ 2019-01-21 16:02:29: Failed to map: 2042
```
```
bgzip esp6500.hg38.vcf
```

**4.** Sort and index **esp6500.hg38.vcf**:

```
zcat esp6500.hg38.vcf.gz | awk '{if(/^#/)print;else exit}' | sed "s/##contig=<ID=/##contig=<ID=chr/g" | sed "s/chrHLA/HLA/g" |  bgzip >> tmp
zcat esp6500.hg38.vcf.gz | grep -v "^#" | sort -k1,1V -k2,2n | awk '{print "chr"$0}' | bgzip >> tmp
mv tmp esp6500.hg38.vcf.gz
tabix -p vcf esp6500.hg38.vcf.gz
```

**5.** Create chromosome-wise VCF annotation files:

**Required**: Biopython

```
zcat esp6500.hg38.vcf.gz | python3 ./create-esp6500-annotation-file.py ./
for i in {1..22}; do tabix -p vcf chr$i.esp6500.vcf.gz; done
tabix -p vcf chrX.esp6500.vcf.gz
tabix -p vcf chrY-and-the-rest.esp6500.vcf.gz
```

**List of ESP6500 INFO fields:**

**Source:** ESP6500
**Version:** ESP6500SI-V2-SSA137.GRCh38-liftover.snps_indels, lifted over with CrossMap v0.3.0

| INFO          | Number | Type | Description                                                        |
|--------------------|-------------|-----------|-------------------------------------------------------------------------|
| ISEQ_ESP6500_EA_AF | A           | Integer   | Alternate allele frequency - ESP6500, European American population (EA) |
| ISEQ_ESP6500_AA_AF | A           | Integer   | Alternate allele frequency - ESP6500, African American population (AA)  |
| ISEQ_ESP6500_AN    | 1           | Integer   | Total allele count - ESP6500                                |
| ISEQ_ESP6500_AF    | A           | Integer   | Alternate allele frequency - ESP6500                                    |


## ExAC

**Last update date:** 21-12-2018  
**Update requirements:** almost never

**1.** Download ExAC database in VCF format, version 1.0,  from  [Broad Institute FTP](ftp://ftp.broadinstitute.org/pub/ExAC_release/release1/):

```
wget -c ftp://ftp.broadinstitute.org/pub/ExAC_release/release1/ExAC.r1.sites.vep.vcf.gz
wget -c ftp://ftp.broadinstitute.org/pub/ExAC_release/release1/ExAC.r1.sites.vep.vcf.gz.tbi
```

**2.**  Download files necessary for liftovering the database from GRCh37/hg19  to hg38:

 * **Homo_sapiens_assembly38.fasta.gz** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

 *  **hg19ToHg38.over.chain.gz** - chain file, from [CrossMap site](http://crossmap.sourceforge.net/)

	```
	wget -c http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz
    ```

**2.**  Perform liftovering using CrossMap.py, version 0.3.0:

```
CrossMap.py vcf hg19ToHg38.over.chain.gz ExAC.r1.sites.vep.vcf.gz Homo_sapiens_assembly38.fa exac.hg38.vcf
```
```
@ 2018-12-23 17:15:03: Read chain_file:  hg19ToHg38.over.chain.gz
@ 2018-12-23 17:15:03: Creating index for Homo_sapiens_assembly38.fasta
@ 2018-12-23 17:17:46: Updating contig field ...
@ 2018-12-23 17:32:10: Total entries: 9362318
@ 2018-12-23 17:32:10: Failed to map: 9426
```
```
bgzip exac.hg38.vcf
```

**3.** Sort  **exac.hg38.vcf.gz**:

```
zcat exac.hg38.vcf.gz | awk '{if(/^##/)print;else exit}' | bgzip >> tmp
zcat exac.hg38.vcf.gz | grep -v "^#" | grep -v "^##contig" | sort -k1,1V -k2,2n | awk '{print "chr"$0}' | bgzip >> tmp
mv tmp exac.hg38.vcf.gz
```

**5.** Create chromosome-wise VCF annotation files:

**Required**: Biopython

```
zcat exac.hg38.vcf.gz | python3 ./create-exac-annotation-file.py ./
for i in {1..22}; do tabix -p vcf chr$i.exac.vcf.gz; done
tabix -p vcf chrX.exac.vcf.gz
tabix -p vcf chrY-and-the-rest.exac.vcf.gz
```
**List of ExAC INFO fields:**

**Source:** ExAC
**Version:** ExAC v.1.0, lifted over to hg38 with CrossMap v0.3.0

| INFO                    | Number | Type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
|-------------------------|--------|---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ_EXAC_AC            | A      | Float   | Alternate allele count for samples (AC_Adj) - ExAC                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_EXAC_AN            | 1      | Integer | Total number of alleles in samples (AN_Adj) - ExAC                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_EXAC_AF            | A      | Integer | Alternate allele frequency in samples (AC_Adj / AN_Adj) - ExAC                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| ISEQ_EXAC_POPMAX        | A      | String  | Population with maximum allele frequency - ExAC                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_EXAC_POPMAX_AF     | A      | Float   | Maximum allele frequency across populations - ExAC                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_EXAC_AFR_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, African / African American population (ARF)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_EXAC_AMR_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, Latino population (AMR)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_EXAC_EAS_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, East Asian population (EAS)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_EXAC_FIN_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, Finnish population (FIN)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_EXAC_NFE_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, Non-Finnish European population (EUR)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_EXAC_OTH_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, Other population (OTH)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_EXAC_SAS_AF        | A      | Float   | Alternate allele frequency in samples - ExAC, South Asian population (SAS)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_EXAC_FILTER_STATUS | .      | String  | Filter status. Possible values: Possible values: PASS (All filters passed), InbreedingCoeff_Filter (InbreedingCoeff <= -0.8), LowQual (Low quality), NewCut_Filter (VQSLOD > -2.632 && InbreedingCoeff >-0.8), VQSRTrancheINDEL95.00to96.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: 0.9503 <= x < 1.2168), VQSRTrancheINDEL96.00to97.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: 0.7622 <= x < 0.9503), VQSRTrancheINDEL97.00to99.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: 0.0426 <= x < 0.7622), VQSRTrancheINDEL99.00to99.50 (Truth sensitivity tranche level for INDEL model at VQS Lod: -0.8363 <= x < 0.0426), VQSRTrancheINDEL99.50to99.90 (Truth sensitivity tranche level for INDEL model at VQS Lod: -8.5421 <= x < -0.8363), VQSRTrancheINDEL99.90to99.95 (Truth sensitivity tranche level for INDEL model at VQS Lod: -18.4482 <= x < -8.5421), VQSRTrancheINDEL99.95to100.00+ (Truth sensitivity tranche level for INDEL model at VQS Lod < -37254.4742), VQSRTrancheINDEL99.95to100.00 (Truth sensitivity tranche level for INDEL model at VQS Lod: -37254.4742 <= x < -18.4482), VQSRTrancheSNP99.60to99.80 (Truth sensitivity tranche level for SNP model at VQS Lod: -4.9627 <= x < -1.8251), VQSRTrancheSNP99.80to99.90 (Truth sensitivity tranche level for SNP model at VQS Lod: -31.4709 <= x < -4.9627), VQSRTrancheSNP99.90to99.95 (Truth sensitivity tranche level for SNP model at VQS Lod: -170.3725 <= x < -31.4709), VQSRTrancheSNP99.95to100.00+ (Truth sensitivity tranche level for SNP model at VQS Lod < -39645.8352), VQSRTrancheSNP99.95to100.00 (Truth sensitivity tranche level for SNP model at VQS Lod: -39645.8352 <= x < -170.3725), AC_Adj0_Filter (No sample passed the adjusted threshold (GQ >= 20 and DP >= 10)) – ExAC |


## gnomAD exomes

**Last update date:** 21-12-2018  
**Update requirements:** almost never

**1.** Download gnomAD exomes database in VCF format, version 2.1, from  [gnomAD download site](http://gnomad.broadinstitute.org/downloads). Description of the version 2.1 can be read in this [blogpost](https://macarthurlab.org/2018/10/17/gnomad-v2-1/):

```
wget -c https://storage.googleapis.com/gnomad-public/release/2.1/vcf/exomes/gnomad.exomes.r2.1.sites.vcf.bgz
wget -c https://storage.googleapis.com/gnomad-public/release/2.1/vcf/exomes/gnomad.exomes.r2.1.sites.vcf.bgz.tbi
mv gnomad.exomes.r2.1.sites.vcf.bgz gnomad.exomes.r2.1.sites.vcf.gz
mv gnomad.exomes.r2.1.sites.vcf.bgz.tbi gnomad.exomes.r2.1.sites.vcf.gz.tbi
```

**2.**  Download files necessary for liftovering the database from GRCh37/hg19  to hg38:

 * **Homo_sapiens_assembly38.fasta.gz** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

 *  **hg19ToHg38.over.chain.gz** - chain file, from [CrossMap site](http://crossmap.sourceforge.net/)

	```
	wget -c http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz
	```

**3.**  Perform liftovering using CrossMap.py, version 0.3.0:

```
CrossMap.py vcf hg19ToHg38.over.chain.gz gnomad.exomes.r2.1.sites.vcf.gz Homo_sapiens_assembly38.fasta gnomad.exomes.r2.1.sites.hg38.vcf
```
```
@ 2018-12-23 18:54:57: Read chain_file:  hg19ToHg38.over.chain.gz
@ 2018-12-23 18:54:57: Updating contig field ...
@ 2018-12-23 20:07:32: Total entries: 17209972
@ 2018-12-23 20:07:32: Failed to map: 21837
```
```
bgzip gnomad.exomes.r2.1.sites.hg38.vcf
```

**4.** Sort and index  **gnomad.exomes.r2.1.sites.hg38.vcf.gz**:

```
zcat gnomad.exomes.r2.1.sites.hg38.vcf.gz | awk '{if(/^#/)print;else exit}' | sed "s/##contig=<ID=/##contig=<ID=chr/g" | sed "s/##contig=<ID=chrHLA/##contig=<ID=HLA/g" |  bgzip >> tmp
zcat gnomad.exomes.r2.1.sites.hg38.vcf.gz | grep -v "^#" | sort -k1,1V -k2,2n | awk '{print "chr"$0}' | bgzip >> tmp
mv tmp gnomad.exomes.r2.1.sites.hg38.vcf.gz
tabix -p vcf gnomad.exomes.r2.1.sites.hg38.vcf.gz
```

**5.** Create chromosome-wise VCF annotation files:

**Required**: Biopython

```
zcat gnomad.exomes.r2.1.sites.hg38.vcf.gz | python3 ./create-gnomad-exomes-annotation-file.py ./
for i in {1..22}; do tabix -p vcf chr$i.genomes-exomes.vcf.gz; done
tabix -p vcf chrX.genomes-exomes.vcf.gz
tabix -p vcf chrY-and-the-rest.genomes-exomes.vcf.gz
```
**List of gnomAD exomes INFO fields:**

**Source:** gnomAD
**Verison:** gnomAD v.2.1, lifted over to hg38 with CrossMap v0.3.0

| INFO                                         | Number | Type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|----------------------------------------------|--------|---------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ_GNOMAD_EXOMES_AC                        | A      | Integer | Alternate allele count for samples - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_AN                        | 1      | Integer | Total number of alleles in samples - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_AF                        | A      | Float   | Alternate allele frequency in samples - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_EXOMES_nhomalt                   | A      | Integer | Count of homozygous individuals in samples - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_controls_AF               | A      | Float   | Alternate allele frequency in samples in the controls subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_EXOMES_controls_nhomalt          | A      | Integer | Count of homozygous individuals in samples in the controls subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF              | A      | Float   | Alternate allele frequency in samples in the non_neuro subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ISEQ_GNOMAD_EXOMES_non_neuro_nhomalt         | A      | Integer | Count of homozygous individuals in samples in the non_neuro subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF             | A      | Float   | Alternate allele frequency in samples in the non_cancer subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_EXOMES_non_cancer_nhomalt        | A      | Integer | Count of homozygous individuals in samples in the non_cancer subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_popmax                    | A      | String  | Population with maximum AF - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_popmax                    | A      | Float   | Alternate allele frequency in samples - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_EXOMES_nhomalt_popmax            | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_controls_popmax           | A      | String  | Population with maximum AF in the controls subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_EXOMES_controls_AF_popmax        | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the controls subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_EXOMES_controls_nhomalt_popmax   | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the controls subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_non_neuro_popmax          | A      | String  | Population with maximum AF in the non_neuro subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_popmax       | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the non_neuro subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_EXOMES_non_neuro_nhomalt_popmax  | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the non_neuro subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_EXOMES_non_cancer_popmax         | A      | String  | Population with maximum AF in the non_cancer subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_popmax      | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the non_cancer subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_non_cancer_nhomalt_popmax | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the non_cancer subset - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_EXOMES_AF_afr                    | A      | Float   | Alternate allele frequency in samples of African-American ancestry (afr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_EXOMES_controls_AF_afr           | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the controls subset (afr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_afr          | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the non_neuro subset (afr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_afr         | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the non_cancer subset (afr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_EXOMES_AF_amr                    | A      | Float   | Alternate allele frequency in samples of Latino ancestry (amr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_EXOMES_controls_AF_amr           | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the controls subset (amr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_amr          | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the non_neuro subset (amr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_amr         | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the non_cancer subset (amr) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_EXOMES_AF_asj                    | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry (asj) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_EXOMES_controls_AF_asj           | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the controls subset (asj) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_asj          | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the non_neuro subset (asj) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_asj         | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the non_cancer subset (asj) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_EXOMES_AF_eas                    | A      | Float   | Alternate allele frequency in samples of East Asian ancestry (eas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_controls_AF_eas           | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the controls subset (eas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_eas          | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the non_neuro subset (eas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_eas         | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the non_cancer subset (eas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_EXOMES_AF_eas_subpopulations     | A      | String  | Alternate allele frequencies in samples of Korean ancestry (kor), Japanese ancestry (jap), non-Korean, non-Japanese East Asian ancestry (oea) in all samples, in the controls subset, in the non_neuro subset and in the non_cancer subset: 'Allele | subpopulation_name : AF_eas_kor : controls_AF_eas_kor : non_neuro_AF_eas_kor : non_cancer_AF_eas_kor : | subpopulation_name : AF_eas_jpn : controls_AF_eas_jpn : non_neuro_AF_eas_jpn : non_cancer_AF_eas_jpn : | subpopulation_name : AF_eas_oea : controls_AF_eas_oea : non_neuro_AF_eas_oea : non_cancer_AF_eas_oea' - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ISEQ_GNOMAD_EXOMES_AF_fin                    | A      | Float   | Alternate allele frequency in samples of Finnish ancestry (fin) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_EXOMES_controls_AF_fin           | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the controls subset (fin) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_fin          | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the non_neuro subset (fin) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_fin         | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the non_cancer subset (fin) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_EXOMES_AF_nfe                    | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry (nfe) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_EXOMES_controls_AF_nfe           | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the controls subset (nfe) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_nfe          | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the non_neuro subset (nfe) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_nfe         | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the non_cancer subset (nfe) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_EXOMES_AF_nfe_subpopulations     | A      | String  | Alternate allele frequencies in samples of  Bulgarian ancestry (bgr), Estonian ancestry (est), North-Western European ancestry (nwe), Southern European ancestry (seu), Swedish ancestry (swe), non-Finnish but otherwise indeterminate European ancestry ancestry (onf) in all samples, in the controls subset, in the non_neuro subset: 'Allele | subpopulation_name : AF_nfe_bgr : controls_AF_nfe_bgr : non_neuro_AF_nfe_bgr : non_cancer_AF_nfe_bgr : | subpopulation_name : AF_nfe_est : controls_AF_nfe_est : non_neuro_AF_nfe_est : non_cancer_AF_nfe_est : | subpopulation_name : AF_nfe_nwe : controls_AF_nfe_nwe : non_neuro_AF_nfe_nwe : non_cancer_AF_nfe_nwe : | subpopulation_name : AF_nfe_seu : controls_AF_nfe_seu : non_neuro_AF_nfe_seu : non_cancer_AF_nfe_seu : | subpopulation_name : AF_nfe_swe : controls_AF_nfe_swe : non_neuro_AF_nfe_swe : non_cancer_AF_nfe_swe : | subpopulation_name : AF_nfe_onf : controls_AF_nfe_onf : non_neuro_AF_nfe_onf : non_cancer_AF_nfe_onf' - gnomAD exomes |
| ISEQ_GNOMAD_EXOMES_AF_sas                    | A      | Float   | Alternate allele frequency in samples of South Asian ancestry (sas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_controls_AF_sas           | A      | Float   | Alternate allele frequency in samples of South Asian ancestry in the controls subset (sas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_sas          | A      | Float   | Alternate allele frequency in samples of South Asian ancestry in the non_neuro subset (sas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_sas         | A      | Float   | Alternate allele frequency in samples of South Asian ancestry in the non_cancer subset (sas) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_EXOMES_AF_oth                    | A      | Float   | Alternate allele frequency in samples of uncertain ancestry (oth) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_EXOMES_controls_AF_oth           | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the controls subset (oth) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_EXOMES_non_neuro_AF_oth          | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the non_neuro subset (oth) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_EXOMES_non_cancer_AF_oth         | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the non_cancer subset (oth) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_EXOMES_segdup                    | 0      | Flag    | Variant falls within a segmental duplication region - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_EXOMES_lcr                       | 0      | Flag    | Variant falls within a low complexity region - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_EXOMES_decoy                     | 0      | Flag    | Variant falls within a reference decoy region - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ISEQ_GNOMAD_EXOMES_nonpar                    | 0      | Flag    | Variant (on sex chromosome) falls outside a pseudoautosomal region - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_EXOMES_FILTER_STATUS             | .      | String  | Filter status. Possible values: PASS (All filters passed), InbreedingCoeff (InbreedingCoeff < -0.3), RF (Failed random forest filtering thresholds of 0.055272738028512555, 0.20641025579497013 (probabilities of being a true positive variant) for SNPs, indels), AC0 (Allele count is zero after filtering out low-confidence genotypes (GQ < 20; DP < 10; and AB < 0.2 for het calls) - gnomAD exomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |


## gnomAD genomes

**1.** Download gnomAD genomes database in VCF format, version 2.1, from  [gnomAD download site](http://gnomad.broadinstitute.org/downloads).  Description of the version 2.1 can be read in this [blogpost](https://macarthurlab.org/2018/10/17/gnomad-v2-1/):

```
for i in {1..22}; do
wget -c https://storage.googleapis.com/gnomad-public/release/2.1/vcf/genomes/gnomad.genomes.r2.1.sites.chr"$i".vcf.bgz;
mv gnomad.genomes.r2.1.sites.chr"$i".vcf.bgz gnomad.genomes.r2.1.sites.chr"$i".vcf.gz;
done

wget -c https://storage.googleapis.com/gnomad-public/release/2.1/vcf/genomes/gnomad.genomes.r2.1.sites.chrX.vcf.bgz;
mv gnomad.genomes.r2.1.sites.chrX.vcf.bgz gnomad.genomes.r2.1.sites.chrX.vcf.gz;
```

**(Optional)** Remove redundant fields (fields with information that will be not included in filal annotation files) to reduce disk space. List of those fields is in **list-of-fields-to-remove.txt**:

```
for i in {1..22}; do
echo "zcat gnomad.genomes.r2.1.sites.chr$i.vcf.gz | ./keep-or-remove-vcf-fields.py -r -f list-of-fields-to-remove.txt | bgzip > gnomad.genomes.r2.1.sites.chr$i.redundant-fields-removed.vcf.gz &"
done

for i in {1..22}; do
mv gnomad.genomes.r2.1.sites.chr$i.redundant-fields-removed.vcf.gz gnomad.genomes.r2.1.sites.chr$i.vcf.gz
done

zcat gnomad.genomes.r2.1.sites.chrX.vcf.gz | ./keep-or-remove-vcf-fields.py -r -f list-of-fields-to-remove.txt | bgzip > gnomad.genomes.r2.1.sites.chrX.redundant-fields-removed.vcf.gz &
mv gnomad.genomes.r2.1.sites.chrX.redundant-fields-removed.vcf.gz gnomad.genomes.r2.1.sites.X.vcf.gz
```

**2.**  Download files necessary for liftovering the database from GRCh37/hg19  to hg38:

 * **Homo_sapiens_assembly38.fasta.gz** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

 *  **hg19ToHg38.over.chain.gz** - chain file, from [CrossMap site](http://crossmap.sourceforge.net/)

	```
	wget -c http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz
	```

**3.**  Perform liftovering using CrossMap.py, version 0.3.0:

```
for i in {1..22}; do
printf "\nChromosome: chr$i\n" > gnomad-genomes.crossmap.chr$i.log
done
printf "\nChromosome: chrX\n" >> gnomad-genomes.crossmap.chrX.log

for i in {1..22}; do
echo "CrossMap.py vcf hg19ToHg38.over.chain.gz gnomad.genomes.r2.1.sites.chr$i.vcf.gz Homo_sapiens_assembly38.fasta gnomad.genomes.r2.1.sites.chr$i.hg38.vcf &> gnomad-genomes.crossmap.chr$i.log &"
done
echo "CrossMap.py vcf hg19ToHg38.over.chain.gz gnomad.genomes.r2.1.sites.chrX.vcf.gz Homo_sapiens_assembly38.fasta gnomad.genomes.r2.1.sites.chrX.hg38.vcf &>> gnomad-genomes.crossmap.chrX.log &"

touch gnomad-genomes.crossmap.log
for i in {1..22}; do
cat gnomad-genomes.crossmap.chr$i.log >> gnomad-genomes.crossmap.log
rm gnomad-genomes.crossmap.chr$i.log
done

cat gnomad-genomes.crossmap.chr$X.log >> gnomad-genomes.crossmap.log
rm gnomad-genomes.crossmap.chrX.log

for i in {1..22}; do
	bgzip gnomad.genomes.r2.1.sites.chr$i.hg38.vcf
done
bgzip gnomad.genomes.r2.1.sites.chrX.hg38.vcf
```

**4.** Sort and bgzip chromosome-wise liftovered files::

```
for i in {1..22}; do
echo "zcat gnomad.genomes.r2.1.sites.chr$i.hg38.vcf.gz | awk '{if(/^#/)print;else exit}' | sed \"s/##contig=<ID=/##contig=<ID=chr/g\" | sed \"s/chrHLA/HLA/g\" | bgzip >> chr$i.tmp.gz &"
echo "zcat gnomad.genomes.r2.1.sites.chr$i.hg38.vcf.gz | grep -v \"^#\" | sort -k1,1V -k2,2n | awk '{print \"chr\"\$0}' | bgzip >> chr$i.tmp.gz &"
done

for i in {1..22}; do
echo "mv chr$i.tmp.gz gnomad.genomes.r2.1.sites.chr$i.hg38.vcf.gz"
done

i="X"
zcat gnomad.genomes.r2.1.sites.chr"$i".hg38.vcf.gz | awk '{if(/^#/)print;else exit}' | sed "s/##contig=<ID=/##contig=<ID=chr/g" | sed "s/chrHLA/HLA/g" | bgzip > chr$i.tmp.gz
zcat gnomad.genomes.r2.1.sites.chr"$i".hg38.vcf.gz | grep -v "^#" | sort -k1,1V -k2,2n | awk '{print "chr"$0}' | bgzip >> chr$i.tmp.gz
mv chr$i.tmp.gz gnomad.genomes.r2.1.sites.chr"$i".hg38.vcf.gz
```

**5.** Create chromosome-wise VCF annotation files, sort and index them:

**Required**: Biopython

```
zcat gnomad.genomes.r2.1.sites.chr*.hg38.vcf.gz | python3 ./create-gnomad-genomes-annotation-file.py ./
```
```
for i in {1..22}; do
	zcat chr$i.gnomad-genomes.vcf.gz | awk '{if(/^#/)print;else exit}' | bgzip > chr$i.tmp.gz
done
zcat chrX.gnomad-genomes.vcf.gz  | awk '{if(/^#/)print;else exit}' | bgzip > chrX.tmp.gz
zcat chrY-and-the-rest.gnomad-genomes.vcf.gz  | awk '{if(/^#/)print;else exit}' | bgzip > chrY-and-the-rest.tmp.gz

for i in {1..22}; do
	echo "zcat chr$i.gnomad-genomes.vcf.gz | grep -v \"^#\" | sort -k1,1V -k2,2n | bgzip >> chr$i.tmp.gz &"
done
echo "zcat chrX.gnomad-genomes.vcf.gz | grep -v \"^#\" | sort -k1,1V -k2,2n | bgzip >> chrX.tmp.gz &"
echo "zcat chrY-and-the-rest.gnomad-genomes.vcf.gz | grep -v \"^#\" | sort -k1,1V -k2,2n | bgzip >> chrY-and-the-rest.tmp.gz &"

for i in {1..22}; do
echo "mv chr$i.tmp.gz chr$i.gnomad-genomes.vcf.gz"
done
echo "mv chrX.tmp.gz chrX.gnomad-genomes.vcf.gz"
echo "mv chrY-and-the-rest.tmp.gz chrY-and-the-rest.gnomad-genomes.vcf.gz"
```
```
for i in {1..22}; do tabix -p vcf chr$i.gnomad-genomes.vcf.gz; done
tabix -p vcf chrX.gnomad-genomes.vcf.gz
tabix -p vcf chrY-and-the-rest.gnomad-genomes.vcf.gz
```

**List of  gnomAD genomes INFO fields:**

**Source:** gnomAD
**Version:** gnomAD v.2.1, lifted over to hg38 with CrossMap v0.3.0

| INFO                                         | Number | Type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|----------------------------------------------|--------|---------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ_GNOMAD_GENOMES_AC                       | A      | Integer | Alternate allele count for samples - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_AN                       | 1      | Integer | Total number of alleles in samples -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_AF                       | A      | Float   | Alternate allele frequency in samples - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_GENOMES_nhomalt                  | A      | Integer | Count of homozygous individuals in samples - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_controls_AF              | A      | Float   | Alternate allele frequency in samples in the controls subset -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_GENOMES_controls_nhomalt         | A      | Integer | Count of homozygous individuals in samples in the controls subset -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF             | A      | Float   | Alternate allele frequency in samples in the non_neuro subset -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_GENOMES_non_neuro_nhomalt        | A      | Integer | Count of homozygous individuals in samples in the non_neuro subset -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_popmax                   | A      | String  | Population with maximum AF -  gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_popmax_AF                | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_GENOMES_nhomalt_popmax           | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_controls_popmax          | A      | String  | Population with maximum AF in the controls subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_GENOMES_controls_AF_popmax       | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the controls subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_nhomalt_popmax  | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the controls subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_non_neuro_popmax         | A      | String  | Population with maximum AF in the non_neuro subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_popmax      | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the non_neuro subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_GENOMES_non_neuro_nhomalt_popmax | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the non_neuro subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_AF_afr                   | A      | Float   | Alternate allele frequency in samples of African-American ancestry (afr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_AF_afr          | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the controls subset (afr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_afr         | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the non_neuro subset (afr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_GENOMES_AF_amr                   | A      | Float   | Alternate allele frequency in samples of Latino ancestry (amr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_GENOMES_controls_AF_amr          | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the controls subset (amr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_amr         | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the non_neuro subset (amr) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_GENOMES_AF_asj                   | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry (asj) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_AF_asj          | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the controls subset (asj) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_asj         | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the non_neuro subset (asj) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_GENOMES_AF_eas                   | A      | Float   | Alternate allele frequency in samples of East Asian ancestry (eas) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_controls_AF_eas          | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the controls subset (eas) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_eas         | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the non_neuro subset (eas) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_AF_fin                   | A      | Float   | Alternate allele frequency in samples of Finnish ancestry (fin) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_controls_AF_fin          | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the controls subset (fin) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_fin         | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the non_neuro subset  (fin) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_AF_nfe                   | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry (nfe) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_GENOMES_controls_AF_nfe          | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the controls subset (nfe) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_nfe         | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the non_neuro subset (nfe) - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ISEQ_GNOMAD_GENOMES_AF_oth                   | A      | Float   | Alternate allele frequency in samples of uncertain ancestry - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_controls_AF_oth          | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the controls subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_oth         | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the non_neuro subset - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_AF_nfe_subpopulations    | A      | String  | Alternate allele frequencies in samples of Estonian ancestry (est), North-Western European ancestry (nwe), Southern European ancestry (seu), non-Finnish but otherwise indeterminate European ancestry ancestry (onf) in all samples, in the controls subset and in the non_neuro subset: 'Allele | subpopulation_name : AF_nfe_est : controls_AF_nfe_est : non_neuro_AF_nfe_est | subpopulation_name : AF_nfe_nwe : controls_AF_nfe_nwe: non_neuro_AF_nfe_nwe | subpopulation_name : AF_nfe_seu : controls_AF_nfe_seu : non_neuro_AF_nfe_seu | subpopulation_name : AF_nfe_onf : controls_AF_nfe_onf : non_neuro_AF_nfe_onf' - gnomAD genomes |
| ISEQ_GNOMAD_GENOMES_ segdup                  | 0      | Flag    | Variant falls within a segmental duplication region - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_ lcr                     | 0      | Flag    | Variant falls within a low complexity region - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_GENOMES_ decoy                   | 0      | Flag    | Variant falls within a reference decoy region - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_GENOMES_ nonpar                  | 0      | Flag    | Variant (on sex chromosome) falls outside a pseudoautosomal region - gnomAD genomes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_FILTER_STATUS            | .      | String  | Filter status. Possible values: PASS (All filters passed), InbreedingCoeff (InbreedingCoeff < -0.3), RF (failed random forest filtering thresholds of 0.2634762834546574, 0.22213813189901457 (probabilities of being a true positive variant) for SNPs, indels), AC0 (Allele count is zero after filtering out low-confidence genotypes (GQ < 20; DP < 10; and AB < 0.2 for het calls) - gnomAD genomes                                                                                                                                                                                                        |


## gnomAD genomes - exome calling intervals

**1.** Download gnomAD genomes (exome calling intervals) database in VCF format, version 2.1, from  [gnomAD download site](http://gnomad.broadinstitute.org/downloads). Description of the version 2.1 can be read in this [blogpost](https://macarthurlab.org/2018/10/17/gnomad-v2-1/):

```
wget -c https://storage.googleapis.com/gnomad-public/release/2.1/vcf/genomes/gnomad.genomes.r2.1.exome_calling_intervals.sites.vcf.bgz
mv gnomad.genomes.r2.1.exome_calling_intervals.sites.vcf.bgz gnomad.genomes.r2.1.exome_calling_intervals.sites.vcf.gz
```

**2.**  Download files necessary for liftovering the database from GRCh37/hg19  to hg38:

 * **Homo_sapiens_assembly38.fasta.gz** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

 *  **hg19ToHg38.over.chain.gz** - chain file, from [CrossMap site](http://crossmap.sourceforge.net/)

	```
	wget -c http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz
	```

**3.**  Perform liftovering using CrossMap.py, version 0.3.0:

```
CrossMap.py vcf hg19ToHg38.over.chain.gz gnomad.genomes.r2.1.exome_calling_intervals.sites.vcf.gz Homo_sapiens_assembly38.fasta gnomad.genomes.r2.1.exome_calling_intervals.sites.hg38.vcf
@ 2019-01-07 14:12:17: Read chain_file:  hg19ToHg38.over.chain.gz
@ 2019-01-07 14:12:18: Creating index for Homo_sapiens_assembly38.fasta
@ 2019-01-07 14:12:38: Updating contig field ...
@ 2019-01-07 14:28:09: Total entries: 4887049
@ 2019-01-07 14:28:09: Failed to map: 8509
```

**4.** Sort, bgzip and index  **gnomad.genomes.r2.1.exome_calling_intervals.sites.hg38.vcf**:

```
cat gnomad.genomes.r2.1.exome_calling_intervals.sites.hg38.vcf | awk '{if(/^#/)print;else exit}' | sed "s/##contig=<ID=/##contig=<ID=chr/g" | sed "s/##contig=<ID=chrHLA/##contig=<ID=HLA/g" | bgzip >> tmp
cat gnomad.genomes.r2.1.exome_calling_intervals.sites.hg38.vcf | grep -v "^#" | sort -k1,1V -k2,2n | awk '{print "chr"$0}' | bgzip >> tmp
mv tmp gnomad.genomes.r2.1.exome_calling_intervals.sites.hg38.vcf.gz
tabix -p vcf gnomad.genomes.r2.1.exome_calling_intervals.sites.hg38.vcf.gz
rm tmp gnomad.genomes.r2.1.exome_calling_intervals.sites.hg38.vcf
```

**5.** Create chromosome-wise VCF annotation files:

**Required**: Biopython

```
zcat gnomad.genomes.r2.1.exome_calling_intervals.sites.hg38.vcf.gz | python3 ./create-gnomad-genomes-exome-calling-intervals-annotation-file.py ./
for i in {1..22}; do tabix -p vcf chr$i.gnomad-genomes-exome-calling-intervals.vcf.gz; done
tabix -p vcf chrX.gnomad-genomes-exome-calling-intervals.vcf.gz
tabix -p vcf chrY-and-the-rest.gnomad-genomes-exome-calling-intervals.vcf.gz
```

**List of  gnomAD genomes - exome calling intervals INFO fields:**

**Source:** gnomAD
**Version:** gnomAD v.2.1, lifted over to hg38 with CrossMap v0.3.0

| INFO                                         | Number | Type    | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
|----------------------------------------------|--------|---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ_GNOMAD_GENOMES_AC                       | A      | Integer | Alternate allele count for samples - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_AN                       | 1      | Integer | Total number of alleles in samples -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_AF                       | A      | Float   | Alternate allele frequency in samples - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_GENOMES_nhomalt                  | A      | Integer | Count of homozygous individuals in samples - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_controls_AF              | A      | Float   | Alternate allele frequency in samples in the controls subset -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_GENOMES_controls_nhomalt         | A      | Integer | Count of homozygous individuals in samples in the controls subset -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF             | A      | Float   | Alternate allele frequency in samples in the non_neuro subset -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_GENOMES_non_neuro_nhomalt        | A      | Integer | Count of homozygous individuals in samples in the non_neuro subset -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_popmax                   | A      | String  | Population with maximum AF -  gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_popmax_AF                | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_GENOMES_nhomalt_popmax           | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_controls_popmax          | A      | String  | Population with maximum AF in the controls subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ISEQ_GNOMAD_GENOMES_controls_AF_popmax       | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the controls subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_nhomalt_popmax  | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the controls subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_non_neuro_popmax         | A      | String  | Population with maximum AF in the non_neuro subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_popmax      | A      | Float   | Maximum allele frequency across populations (excluding samples of Ashkenazi, Finnish, and indeterminate ancestry) in the non_neuro subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_GENOMES_non_neuro_nhomalt_popmax | A      | Integer | Count of homozygous individuals in the population with the maximum allele frequency in the non_neuro subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_AF_afr                   | A      | Float   | Alternate allele frequency in samples of African-American ancestry (afr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_AF_afr          | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the controls subset (afr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_afr         | A      | Float   | Alternate allele frequency in samples of African-American ancestry in the non_neuro subset (afr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_GENOMES_AF_amr                   | A      | Float   | Alternate allele frequency in samples of Latino ancestry (amr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ISEQ_GNOMAD_GENOMES_controls_AF_amr          | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the controls subset (amr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_amr         | A      | Float   | Alternate allele frequency in samples of Latino ancestry in the non_neuro subset (amr) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_GENOMES_AF_asj                   | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry (asj) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_controls_AF_asj          | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the controls subset (asj) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_asj         | A      | Float   | Alternate allele frequency in samples of Ashkenazi Jewish ancestry in the non_neuro subset (asj) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ISEQ_GNOMAD_GENOMES_AF_eas                   | A      | Float   | Alternate allele frequency in samples of East Asian ancestry (eas) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_controls_AF_eas          | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the controls subset (eas) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_eas         | A      | Float   | Alternate allele frequency in samples of East Asian ancestry in the non_neuro subset (eas) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ISEQ_GNOMAD_GENOMES_AF_fin                   | A      | Float   | Alternate allele frequency in samples of Finnish ancestry (fin) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| ISEQ_GNOMAD_GENOMES_controls_AF_fin          | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the controls subset (fin) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_fin         | A      | Float   | Alternate allele frequency in samples of Finnish ancestry in the non_neuro subset  (fin) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| ISEQ_GNOMAD_GENOMES_AF_nfe                   | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry (nfe) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_GENOMES_controls_AF_nfe          | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the controls subset (nfe) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_nfe         | A      | Float   | Alternate allele frequency in samples of non-Finnish European ancestry in the non_neuro subset (nfe) - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| ISEQ_GNOMAD_GENOMES_AF_oth                   | A      | Float   | Alternate allele frequency in samples of uncertain ancestry - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ISEQ_GNOMAD_GENOMES_controls_AF_oth          | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the controls subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_non_neuro_AF_oth         | A      | Float   | Alternate allele frequency in samples of uncertain ancestry in the non_neuro subset - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_AF_nfe_subpopulations    | A      | String  | Alternate allele frequencies in samples of Estonian ancestry (est), North-Western European ancestry (nwe), Southern European ancestry (seu), non-Finnish but otherwise indeterminate European ancestry ancestry (onf) in all samples, in the controls subset and in the non_neuro subset: 'Allele | subpopulation_name : AF_nfe_est : controls_AF_nfe_est : non_neuro_AF_nfe_est | subpopulation_name : AF_nfe_nwe : controls_AF_nfe_nwe: non_neuro_AF_nfe_nwe | subpopulation_name : AF_nfe_seu : controls_AF_nfe_seu : non_neuro_AF_nfe_seu | subpopulation_name : AF_nfe_onf : controls_AF_nfe_onf : non_neuro_AF_nfe_onf' - gnomAD genomes - exome calling intervals |
| ISEQ_GNOMAD_GENOMES_segdup                  | 0      | Flag    | Variant falls within a segmental duplication region - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ISEQ_GNOMAD_GENOMES_lcr                     | 0      | Flag    | Variant falls within a low complexity region - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ISEQ_GNOMAD_GENOMES_decoy                   | 0      | Flag    | Variant falls within a reference decoy region - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ISEQ_GNOMAD_GENOMES_nonpar                  | 0      | Flag    | Variant (on sex chromosome) falls outside a pseudoautosomal region - gnomAD genomes - exome calling intervals                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ISEQ_GNOMAD_GENOMES_FILTER_STATUS            | .      | String  | Filter status. Possible values: PASS (All filters passed), InbreedingCoeff (InbreedingCoeff < -0.3), RF (failed random forest filtering thresholds of 0.2634762834546574, 0.22213813189901457 (probabilities of being a true positive variant) for SNPs, indels), AC0 (Allele count is zero after filtering out low-confidence genotypes (GQ < 20; DP < 10; and AB < 0.2 for het calls) - gnomAD genomes – exome calling intervals                                                                                                                                                                                                                                       |


## Mitomap

**Last update date:** 21-12-2018  
**Update requirements:** every few months

**1.** Download MITOMAP database in VCF format from  [MITOMAP  Resources](https://mitomap.org/foswiki/bin/view/MITOMAP/Resources):

```
wget http://mitomap.org/cgi-bin/polymorphisms.cgi?format=vcf
mv polymorphisms.cgi\?format\=vcf mitomap.vcf
```

**2.** Create annotation file with **create-mitomap-annotation-file.py** script:

```
cat mitomap.vcf | python3 create-mitomap-annotation-file.py  | bgzip > mitomap.chrY-and-the-rest.vcf.gz
rm mitomap.vcf
tabix -p vcf mitomap.chrY-and-the-rest.vcf.gz
```

The script performs the following actions:

- formats he contents of INFO field "Disease" :

  + converts AF to fraction
  + changes AC and AF fields names to MITOMAP_AC and MITOMAP_AF
  + changes chromosome naming convention from MT to chrM
  + modifies MT contig header line

** List of MITOMAP INFO fields**

**Source:** MITOMAP
**Version:** 2018-12-21

| INFO       | Number | Type  | Description                                              |
|------------|--------|-------|----------------------------------------------------------|
| MITOMAP_AC | A      | Float | Allele count in GenBank out of 32059 sequences – MITOMAP |
| MITOMAP_AF | A      | Float | Allele Frequency in Genbank – MITOMAP                    |


# Functional annotations - variant level


## dbSNP

**Last update date:** 22-01-2019  
**Update requirements:** every few months

**1.**  Download newest dbSNP database in VCF  format from [NCBI ftp site](ftp://ftp.ncbi.nlm.nih.gov/snp/organisms/human_9606/VCF/):

```
wget -c ftp://ftp.ncbi.nlm.nih.gov/snp/organisms/human_9606/VCF/All_20180418.vcf.gz
```

**2.**  Create VCF annotation file:

```
printf "##fileformat=VCFv4.0\n" | bgzip > dbsnp.vcf.gz
printf "##reference=GRCh38.p7\n" | bgzip >> dbsnp.vcf.gz
printf "##INFO=<ID=ISEQ_DBSNP,Number=0,Type=Flag,Description=\"Variant present in NCBI dbSNP\",Source=\"dbSNP\",Version=\"dbSNP Build 151\">\n" | bgzip >> dbsnp.vcf.gz
printf "#CHROM\tPOS\tID	REF\tALT\tQUAL\tFILTER\tINFO\n" | bgzip >> dbsnp.vcf.gz

zcat All_20180418.vcf.gz | grep -v "^#" | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\tISEQ_DBSNP"}' | bgzip >> dbsnp.vcf.gz

tabix -p vcf dbsnp.vcf.gz

rm All_20180418.vcf.gz
```
**List of dbSNP INFO fields**

**Source:**  dbSNP
**Version:** dbSNP Build 151

| INFO       | Number | Type | Description                   |
|------------|--------|------|-------------------------------|
| ISEQ_DBSNP | 0      | Flag | Variant present in NCBI dbSNP |


## ClinVar

**Last update date:** 28-02-2019
**Update requirements:** monthly

**1.**  Download newest ClinVar database in VCF  format, **submission_summary.txt.gz** and **variation_allele.txt.gz** from [ClinVar FTP site](ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/):

```
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz.md5
md5sum clinvar.vcf.gz
cat clinvar.vcf.gz.md5
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/submission_summary.txt.gz
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/submission_summary.txt.gz.md5
md5sum submission_summary.txt.gz
cat submission_summary.txt.gz.md5
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/variation_allele.txt.gz
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/tab_delimited/variation_allele.txt.gz.md5
md5sum variation_allele.txt.gz
cat variation_allele.txt.gz.md5
```

**2.**  Replace troublesome ASCII characters in **clinvar.vcf.gz**:

The downloaded file contains troublesome ASCII characters that cannot be handled properly by some software. Those characters need to be replaced:

```
zcat clinvar.vcf.gz | grep --color='auto' -P -o "[\x80-\xFF]" | sort | uniq
```
```
ä
ã
é
è
ë
ô
ö
ß
ú
ü
```
```
zcat clinvar.vcf.gz | sed 's/ä/a/g' | sed 's/ã/a/g' | sed 's/é/e/g' | sed 's/è/e/g' | sed 's/ë/e/g' | sed 's/ô/o/g' | sed 's/ö/o/g' | sed 's/ß/B/g'| sed 's/ú/u/g' | sed 's/ü/u/g' | bgzip > tmp
mv tmp clinvar.vcf.gz
```

**3.** Create annotation file with **create-clinvar-annotation-file.py** script:

```
gunzip variation_allele.txt.gz
gunzip submission_summary.txt.gz
zcat clinvar.vcf.gz | python3 create-clinvar-annotation-file.py variation_allele.txt submission_summary.txt | bgzip > clinvar.variant-level.vcf.gz
tabix -p vcf clinvar.variant-level.vcf.gz
bgzip variation_allele.txt
bgzip submission_summary.txt
```

The  **clinvar.variant-level.vcf.gz** file contains the following fields from original **clinvar.vcf.gz** file:

- CLNDN field renamed to **ISEQ_VARIANT_CLINVAR_DISEASES**
- CLNDNINCL renamed to **ISEQ_VARIANT_CLINVAR_DISEASES_INCLUDED**
- CLNSIG renamed to **ISEQ_CLNSIG**
- CLNSIGINCL renamed to **ISEQ_CLNSIGINCL**
- CLNREVSTAT renamed to **ISEQ_CLINVAR_REVIEV_STATUS**

with delimiters replaced in the following manner:

- ':' with '|'
- ',' with ','

It also contains the following additional fields created with the use of **submission_summary.txt** file:
- **ISEQ_CLINVAR_SIGNIFICANCE** - Clinical significances from variant_summary.txt
- **ISEQ_CLINVAR_SIGNIFICANCE_INCLUDED** - Clinical significances from variant_summary.txt

and fields created with the use of **variation_alllele.txt** file:
- **ISEQ_CLINVAR_ALLELE_ID**
- **ISEQ_CLINVAR_VARIATION_ID**


**List of ClinVar INFO fields**

**Source:** ClinVar
**Version:** 2019-02-25

| INFO                                   | Number | Type   | Description                                                                                                                                                                  |
|----------------------------------------|--------|--------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ISEQ_CLINVAR_ALLELE_ID                 | 1      | String | ClinVar Allele ID                                                                                                                                                            |
| ISEQ_CLINVAR_VARIATION_ID              | .      | String | ClinVar Variation IDs                                                                                                                                                        |
| ISEQ_VARIANT_CLINVAR_DISEASES          | .      | String | Diseases associated with the variant in ClinVar                                                                                                                              |
| ISEQ_VARIANT_CLINVAR_DISEASES_INCLUDED | .      | String | Diseases associated with a haplotype or genotype that includes the variant in ClinVar                                                                                        |
| ISEQ_CLNSIG                            | .      | String | Clinical significance for the variant in ClinVar                                                                                                                             |
| ISEQ_CLINVAR_SIGNIFICANCE              | .      | String | Clinical significance for the variant in ClinVar, based on variant_summary.txt                                                                                               |
| ISEQ_CLNSIGINCL                        | .      | String | Clinical significance for a haplotype or genotype that includes the variant in ClinVar. Reported as pairs: VariationID \| clinical significance                               |
| ISEQ_CLINVAR_SIGNIFICANCE_INCLUDED     | .      | String | Clinical significance for a haplotype or genotype that includes the variant in ClinVar, based on variant_summary.txt. Reported as pairs: VariationID \| clinical significance |
| ISEQ_CLINVAR_REVIEW_STATUS             | .      | String | ClinVar review status for the Variation ID                                                                                                                                   |


## Mitomap - diseases

**Last update date:** 04-12-2018  
**Update requirements:** every few months.

**1.** Download MITOMAP database in VCF format form  [MITOMAP  Resources](https://mitomap.org/foswiki/bin/view/MITOMAP/Resources)

```
wget http://mitomap.org/cgi-bin/disease.cgi?format=vcf
mv disease.cgi\?format\=vcf  disease.vcf
```

**2.** Create  **mitomap-diseases-abbreviations-dictionary.txt**  file:

Dictionary of MITOMAP abbreviations of diseases  created by copying the content from: http://mitomap.org/foswiki/bin/view/MITOMAP/DiseaseList, with the following amendments applied:

- item "LD: Leigh Disease (or LS: Leigh Syndrome)" was split into two records - "LD: Leigh Disease" and "LS: Leigh Syndrome"
- item "MERME: MERRF/MELAS, Lactic Acidosis, and Stroke-like episodes overlap disease" was changed to "MERME: Myoclonic Epilepsy and Ragged Red Muscle Fibers/Mitochondrial Encephalomyopathy, Lactic Acidosis, and Stroke-like episodes overlap disease"
- item "NARP : Neurogenic muscle weakness, Ataxia, and Retinitis Pigmentosa; alternate phenotype at this locus is reported as Leigh Disease" was changed to "NARP : Neurogenic muscle weakness, Ataxia, and Retinitis Pigmentosa"
- item "Multisystem Mitochondrial Disorder (myopathy, encephalopathy, blindness, hearing loss, peripheral neuropathy)" was removed

Additinally, the following items were added:

- HCM : Hypertrophic CardioMyopathy
- PEO: Progressive External Ophthalmoplegia
- PD: Parkinson Disease
- FSHD: Facioscapulohumeral Muscular Dystrophy
- MIDM: Maternally Inherited Diabetes Mellitus
- MDD: Major Depressive Disorder
- BD: Bipolar Disorder
- SZ: Schizophrenia
- BSN: Bilateral Striatal Necrosis
- MR: Mental Retardation
- DM: Diabetes Mellitus
- NSHL: Non-Syndromic Hearing Loss
- RP: Retinitis Pigmentosa
- CM: Cardiomiopathy

**3.** Create annotation file with **create-mitomap-diseases-annotation-file.py** script:

```
cat disease.vcf | python3 create-mitomap-diseases-annotation-file.py mitomap-diseases-abbreviations-dictionary.txt > mitomap-diseases.variant-level.vcf
bgzip mitomap-diseases.variant-level.vcf
tabix -p vcf mitomap-diseases.variant-level.vcf.gz
```

The script performs the following actions:

- formats the contents of INFO field "Disease" :

  + changes its name to MITOMAP_DISEASE
  + replaces disease abbreviations with disease names
  + replaces '+', '&' and '/' with ','
  + replaces whitespaces with '_'

- formats the contents of INFO field "DiseaseStatus" :

  + changes its name to MITOMAP_DISEASE_STATUS
  + replaces whitespaces with '_'

- removes AF and AC fields
- changes chromosome naming convention from MT to chrM.

**List of MITOMAP INFO fields**

**Source:** MITOMAP
**Version:** 2019-01-24

| INFO                   | Number | Type   | Description                            |
|------------------------|--------|--------|----------------------------------------|
| MITOMAP_DISEASE        | .      | String | Putative Disease Association - MITOMAP |
| MITOMAP_DISEASE_STATUS | .      | Float  | Disease Association Status - MITOMAP   |


# Functional annotations - gene level

## ClinVar - diseases

**Last update date:** 28-02-2019
**Update requirements:** monthly

**1.**  Download newest  **gene_condition_source_id** from [NCBI FTP site](ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/):
```
wget ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/gene_condition_source_id
md5sum gene_condition_source_id
```

**2.**  Create **clinvar-diseases-dictionary.dict** file:

The downloaded **gene_condition_source_id**  file contains troublesome ascii characters that cannot be handled properly by some software. Those characters need to be replaced:

```
cat gene_condition_source_id | grep --color='auto' -P -o "[\x80-\xFF]" | sort | uniq
```
```
é
è
ë
ö
```
This file also contains two columns - "AssociatedGenes" and "RelatedGenes". Those columns are concatenated. The dictionary  **clinvar-diseases.dictionary** file is created with the use of **create-a-dictionary.py** script:

```
DATE="2019-02-28"
printf "##INFO=<ID=ISEQ_GENES_SYMBOLS,Number=.,Type=String,Description=\"Genes symbols\",Source=\"ClinVar\",Version=\"$DATE\">\n" > clinvar-diseases.dictionary
printf "##INFO=<ID=ISEQ_CLINVAR_DISEASES,Number=.,Type=String,Description=\"Names of diseases associated in ClinVar with given genes\",Source=\"ClinVar\",Version=\"$DATE\">\n" >> clinvar-diseases.dictionary

cat gene_condition_source_id | sed 's/ë/e/g' | sed 's/é/e/g' | sed 's/è/e/g'  | sed 's/ö/o/g' | awk -F "\t" '{print $1"\t"$2$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9}'  | sed "s/AssociatedGenesRelatedGenes/AssociatedGenes\/RelatedGenes/g" | python3 create-a-dictionary.py 1 3 -c >> clinvar-diseases.dictionary
```

**List of ClinVar - diseases INFO fields**

**Source:** ClinVar
**Version:** 2019-01-23

| INFO                  | Number | Type   | Description                                                                                         |
|-----------------------|--------|--------|-----------------------------------------------------------------------------------------------------|
| ISEQ_CLINVAR_DISEASES | .      | String | Names of diseases associated with genes that overlap the variant - ClinVar                          |


## Human Phenotype Ontology

**Last update date:** 24-01-2019
**Update requirements:** approximately monthly

**1.** Download files **phenotype_annotation.tab**, **genes_to_diseases.txt**, **ALL_SOURCES_ALL_FREQUENCIES_diseases_to_genes_to_phenotypes.txt**:

The files were downloaded from [HPO Hudson/Jenkins page](http://compbio.charite.de/jenkins/job/hpo.annotations/lastStableBuild/)
```
wget -c http://compbio.charite.de/jenkins/job/hpo.annotations/lastSuccessfulBuild/artifact/misc/phenotype_annotation.tab
wget -c http://compbio.charite.de/jenkins/job/hpo.annotations.monthly/lastSuccessfulBuild/artifact/annotation/genes_to_diseases.txt
wget -c http://compbio.charite.de/jenkins/job/hpo.annotations.monthly/lastSuccessfulBuild/artifact/annotation/ALL_SOURCES_ALL_FREQUENCIES_genes_to_phenotype.txt
```

The downloaded **phenotype_annotation.tab** file contains troublesome ascii characters that cannot be handled properly by some software. Those characters need to be replaced.  The file also containes few corrupted records - those records are fixed with sed:
- PMID:9557891PMID:9557891 to PMID:9557891
- PMID:12687501:PMID:17918734 to PMID:12687501,PMID:17918734

```
cat phenotype_annotation.tab | grep --color='auto' -P -o "[\x80-\xFF]" | sort | uniq
```
```
á
Å
ç
é
è
í
ï
ó
ö
ü
```
```
cat phenotype_annotation.tab | sed 's/á/a/g' | sed 's/Å/A/g' | sed 's/ç/c/g' | sed 's/é/e/g' | sed 's/è/e/g' | sed 's/í/i/g' | sed 's/ï/i/g' | sed 's/ó/o/g' | sed 's/ö/o/g' | sed 's/ü/u/g' | sed 's/PMID:9557891PMID:9557891/PMID:9557891/g' | sed 's/PMID:12687501:PMID:17918734/PMID:12687501,PMID:17918734/g' > tmp
mv tmp phenotype_annotation.tab
```

**2.**  Create **list-of-modes-of-inheritance.txt** file:

This is a manually created file containing tab delimited names and HPO IDs of subclasses of HPO class 'Mode of inheritance' (HP:0000005).

**3.** Create dictionaries:

+ **disease-id-to-diseases.dictionary**
+ **gene-symbol-to-diseases-ids.dictionary**
+ **gene-symbol-to-diseases.dictionary**
+ **gene-symbol-to-phenotypes.dictionary**
+ **gene-symbol-to-modes-of-inheritance.dictionary**

```
DATE="2019-02-28"

printf "##INFO=<ID=ISEQ_HPO_DISEASES_IDS,Number=.,Type=String,Description=\"ID's of diseases associated with given genes - OMIM, Orphanet, and DECIPHER\",Source=\"HPO\",Version=\"$DATE\">\n" > disease-id-to-diseases.dictionary
printf "##INFO=<ID=ISEQ_HPO_DISEASES,Number=.,Type=String,Description=\"Names of diseases from the HPO-team (mostly referring to OMIM), Orphanet, and DECIPHER\",Source=\"HPO\",Version=\"$DATE\">\n" >> disease-id-to-diseases.dictionary

cat phenotype_annotation.tab | python3 create-a-dictionary.py -k ';' -v ';' -d ';' -e  5 2 | python3 create-a-dictionary.py -k ',' -v ';' 0 1 >> disease-id-to-diseases.dictionary

printf "##INFO=<ID=ISEQ_GENES_SYMBOLS,Number=.,Type=String,Description=\"Genes symbols\",Source=\"Ensembl\",Version=\"$DATE\">\n" > gene-symbol-to-diseases-ids.dictionary
printf "##INFO=<ID=ISEQ_HPO_DISEASES_IDS,Number=.,Type=String,Description=\"ID's of diseases associated with given genes - OMIM, Orphanet, and DECIPHER\",Source=\"HPO\",Version=\"$DATE\">\n" >> gene-symbol-to-diseases-ids.dictionary

cat genes_to_diseases.txt | python3 create-a-dictionary.py -c 1 2 >> gene-symbol-to-diseases-ids.dictionary
cat gene-symbol-to-diseases-ids.dictionary | python3 translate-values-of-a-dictionary.py disease-id-to-diseases.dictionary > gene-symbol-to-diseases.dictionary

printf "##INFO=<ID=ISEQ_GENES_SYMBOLS,Number=.,Type=String,Description=\"Genes symbols\",Source=\"Ensembl\",Version=\"$DATE\">\n" > gene-symbol-to-phenotypes.dictionary
printf "##INFO=<ID=ISEQ_HPO_PHENOTYPES,Number=.,Type=String,Description=\"Phenotypes associated with given genes\",Source=\"HPO\",Version=\"$DATE\">\n" >> gene-symbol-to-phenotypes.dictionary

cat ALL_SOURCES_ALL_FREQUENCIES_genes_to_phenotype.txt | python3 create-a-dictionary.py -c 1 2 | python3  filter-modes-of-inheritance.py list-of-modes-of-inheritance.txt >> gene-symbol-to-phenotypes.dictionary

printf "##INFO=<ID=ISEQ_GENES_SYMBOLS,Number=.,Type=String,Description=\"Genes symbols\",Source=\"Ensembl\",Version=\"$DATE\">\n" > gene-symbol-to-modes-of-inheritance.dictionary
printf "##INFO=<ID=ISEQ_HPO_INHERITANCE,Number=.,Type=String,Description=\"Modes of inheritance associated with given genes\",Source=\"HPO\",Version=\"$DATE\">\n" >> gene-symbol-to-modes-of-inheritance.dictionary

cat ALL_SOURCES_ALL_FREQUENCIES_genes_to_phenotype.txt | python3 create-a-dictionary.py -c 1 2 | python3  filter-modes-of-inheritance.py -o list-of-modes-of-inheritance.txt >> gene-symbol-to-modes-of-inheritance.dictionary
```

**List of HPO INFO fields**

**Source:** Human Phenotype Ontology
**Version:** 2019-01-24

| INFO                  | Number | Type   | Description                                                                                         |
|-----------------------|--------|--------|-----------------------------------------------------------------------------------------------------|   
| ISEQ_HPO_DISEASES     | .      | String | Names of diseases associated with genes that overlap the variant - Human Phenotye Ontology (HPO)    |
| ISEQ_HPO_PHENOTYPES   | .      | String | Names of phenotypes associated with genes that overlap the variant - Human Phenotye Ontology (HPO)  |
| ISEQ_HPO_INHERITANCE  | .      | String | Modes of inheritance associated with genes that overlap the variant - Human Phenotye Ontology (HPO) |


# Scores


## CADD

**Last update date:** 06-12-2018  
**Update requirements:** only when new release is available

(In development)



## GERP

**Last update date:** 10-12-2018  
**Update requirements:** only when new release is available

**1.** Download base-wise GERP++ scores (**hg 19, base-wise scores**) from [GERP++ site](http://mendel.stanford.edu/SidowLab/downloads/gerp/) :

```
wget -c http://mendel.stanford.edu/SidowLab/downloads/gerp/hg19.GERP_scores.tar.gz
tar -zxvf hg19.GERP_scores.tar.gz
rm hg19.GERP_scores.tar.gz
```

**2.** Covert **GERP++** database to **bigWig** format:

  * create wig file from chromosome-wise base-wise files
	```
	bash create-wig-file.bash ./ > gerp.hg19.wig
	rm *rates
	```
  * download **wigToBigWig** from [UCSC site with application for Linux](http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/)

	```
	wget -c  http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/wigToBigWig
	chmod +x wigToBigWig
	```
  * create **chrom-sizes**  file and convert **gerp.hg19.wig** to **gerp.hg19.bw**

	```
	bash create-chrom-sizes-file.bash ./ > chrom.sizes
	./wigToBigWig gerp.hg19.wig chrom.sizes gerp.hg19.bw
	rm gerp.hg19.wig
	```

**3.** Liftover **gerp.hg19.bw** to **gerp.grch38.bw**:


 * Download **Homo_sapiens_assembly38.fasta.gz** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)
 *  Download  **hg19ToHg38.over.chain.gz** - chain file, from [CrossMap site](http://crossmap.sourceforge.net/):

	```
	wget -c http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz
	```
 * Liftover using CrossMap v3.0

	```
	CrossMap.py bigwig hg19ToHg38.over.chain.gz hg19ToHg38.over.chain.gz gerp.hg38
	```
	```
	@ 2018-12-10 19:22:33: Read chain_file:  hg19ToHg38.over.chain.gz
	@ 2018-12-10 19:22:33: Liftover bigwig file: gerp.hg19.bw ==> gerp.hgh38.bgr
	@ 2018-12-10 23:36:27: Merging overlapped entries in bedGraph file ...
	@ 2018-12-10 23:36:27: Sorting bedGraph file:gerp.hg38.bgr
	@ 2018-12-11 04:13:33: Writing header to "gerp.hg38.bw" ...
	@ 2018-12-11 04:13:33: Writing entries to "gerp.hg38.bw" ...
	```

**List of GERP++ INFO fields**

**Source:** mendel.stanford.edu/SidowLab/downloads/gerp
**Version:**

| INFO      | Number | Type  | Description                                       |
|-----------|--------|-------|---------------------------------------------------|
| ISEQ_GERP | 1      | Float | GERP score. Lifted over with CrossMap.py (v0.3.0) |


## M-CAP

**Last update date:** 06-12-2018  
**Update requirements:** only when new release is available

**1.** Download necessary files.

 *  **mcap_v1_3.txt.gz** - M-CAP database v1.3, from [M-CAP site](http://bejerano.stanford.edu/mcap/):

	```
	wget -c http://bejerano.stanford.edu/mcap/downloads/dat/mcap_v1_3.txt.gz
	```

 * **Homo_sapiens_assembly38.fasta.gz** - reference genome,  from [GATK data-bundle](https://software.broadinstitute.org/gatk/download/bundle)

 *  **hg19ToHg38.over.chain.gz** - chain file, from [CrossMap site](http://crossmap.sourceforge.net/)

	```
	wget -c http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz
	```

**2.**   Convert M-CAP database to VCF format:

```
printf "##fileformat=VCFv4.2\n" | bgzip > m-cap.grch37.vcf.gz
printf "##fileDate=20181206\n" | bgzip >> m-cap.grch37.vcf.gz
printf "##source=http://bejerano.stanford.edu/mcap/\n" | bgzip >> m-cap.grch37.vcf.gz
printf "##reference=GRCh37/hg19\n" | bgzip >> m-cap.grch37.vcf.gz
printf "##INFO=<ID=ISEQ_M_CAP,Number=1,Type=Float,Description=\"M-CAP score\",Source=\"M-CAP\",Version=\"v1.3, lifted over to hg38 with CrossMap v0.3.0\">\n" | bgzip >> m-cap.grch37.vcf.gz
printf "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n" | bgzip >> m-cap.grch37.vcf.gz

zcat mcap_v1_3.txt.gz | sed "1d" | awk '{print $1"\t"$2"\t.\t"$3"\t"$4"\t.\t.\tISEQ_M_CAP="$5}' | bgzip >> m-cap.grch37.vcf.gz
```

**3.**  Liftover the database from GRCh37/hg19 version to GRCh38 using CrossMap v3.0:

```
CrossMap.py vcf hg19ToHg38.over.chain.gz m-cap.grch37.vcf.gz Homo_sapiens_assembly38.fasta m-cap.hg38.vcf
```
```
@ 2018-12-23 17:58:04: Read chain_file:  hg19ToHg38.over.chain.gz
@ 2018-12-23 17:58:04: Creating index for Homo_sapiens_assembly38.fasta
@ 2018-12-23 17:59:01: Updating contig field ...
@ 2018-12-23 18:33:55: Total entries: 74982522
@ 2018-12-23 18:33:55: Failed to map: 117082
```
```
bgzip m-cap.hg38.vcf
```
**4.**  Sort , fix chromosome naming convention and index **m-cap.hg38.vcf.gz**:

```
printf "##fileformat=VCFv4.2\n" | bgzip > m-cap.hg38.vcf.gz
printf "##INFO=<ID=ISEQ_M_CAP,Number=1,Type=Float,Description=\"M-CAP score\",Source=\"M-CAP\",Version=\"v1.3, lifted over to hg38 with CrossMap v0.3.0\">\n" | bgzip >> m-cap.hg38.vcf.gz
printf "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n" | bgzip >> m-cap.hg38.vcf.gz
zcat m-cap.hg38.vcf.gz | grep -v "^#" | sort -k1,1V -k2,2n | awk '{print "chr"$0}' | bgzip >> tmp
mv tmp m-cap.hg38.vcf.gz
tabix -p vcf m-cap.hg38.vcf.gz
```

**List of M-CAP INFO fields**

**Source:** M-CAP score
**Version:** v1.3, lifted over to hg38 with CrossMap v0.3.0

| INFO       | Number | Type  | Description |
|------------|--------|-------|-------------|
| ISEQ_M_CAP | 1      | Float | M-CAP score |


## PhastCons

**Last update date:** 10-12-2018  
**Update requirements:** only when new release is availible


**1.** Download PhastCons 100way database in bigWig format from [UCSC ftp](http://hgdownload.soe.ucsc.edu/goldenPath/hg38/phastCons100way/):

```
wget -c http://hgdownload.soe.ucsc.edu/goldenPath/hg38/phastCons100way/hg38.phastCons100way.bw
```
**List of PhastCons INFO fields**

**Source:** PhastCons100way
**Version:** PhastCons100way

| INFO      | Number | Type  | Description                  |
|-----------|--------|-------|------------------------------|
| PhastCons100way | 1      | Float | PhastCons100way conservation score |


## PhyloP

**Last update date:** 10-12-2018  
**Update requirements:** only when new release is available


**1.** Download PhyloP100way database in bigWig format from [UCSC ftp](ftp://hgdownload.cse.ucsc.edu/goldenPath/hg38/phyloP100way/):

```
wget -c ftp://hgdownload.cse.ucsc.edu/goldenPath/hg38/phyloP100way/hg38.phyloP100way.bw
```

**List of Phylop INFO fields**

**Source:** PhyloP100way
**Version:** PhyloP100way

| INFO         | Number | Type  | Description        |
|--------------|--------|-------|--------------------|
| PhyloP100way | 1      | Float | PhyloP100way score |


## SIFT

**Last update date:** 05-12-2018  
**Update requirements:** created once for a specific reference genome

**1.** Download precalculated scores from [SIFT site](http://sift.bii.a-star.edu.sg/sift4g/public//Homo_sapiens/):

```
wget -c http://sift.bii.a-star.edu.sg/sift4g/public/Homo_sapiens/GRCh38.83.chr.zip
unzip GRCh38.83.chr.zip
rm GRCh38.83.chr.zip
```
File unzips to a folder **GRCh38.83.chr** with SIFT scores in separate files corresponding to chromosomes.

**2.** Create SIFT annotation file using **create-a-vcf-body-from-sift-chromosome-file.py** script:

```
printf "##fileformat=VCFv4.2\n" | bgzip > sift.vcf.gz
printf "##INFO=<ID=ISEQ_SIFT_MIN,Number=1,Type=Float,Description=\"Minimal SIFT score for a variant\",Source=\"SIFT4G\",Version=\"SIFT4G, GRCh38.83\">\n" | bgzip >> sift.vcf.gz
printf "##INFO=<ID=ISEQ_SIFT,Number=.,Type=String,Description=\"SIFT score annotations: 'Transcript_Id | Gene_Name | Region | HGVS.p | SIFT_score'\",Source=\"SIFT4G\",Version=\"SIFT4G, GRCh38.83\">\n" | bgzip >> sift.vcf.gz
printf "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n" | bgzip >> sift.vcf.gz

SIFT_DIR="./GRCh38.83.chr"

for i in {1..22}; do zcat "$SIFT_DIR"/"$i".gz | python3 create-a-vcf-body-from-sift-chromosome-file.py chr"$i" | bgzip >> sift.vcf.gz ; done

zcat "$SIFT_DIR"/X.gz | cut -f 1,2,3,4,6,7,8,9,10,11 | grep -v "NA$" | grep -v "^#" | python3 create-a-vcf-body-from-sift-chromosome-file.py chrX | bgzip >> sift.vcf.gz

zcat "$SIFT_DIR"/Y.gz | cut -f 1,2,3,4,6,7,8,9,10,11 | grep -v "NA$" | grep -v "^#" | python3 create-a-vcf-body-from-sift-chromosome-file.py chrY | bgzip >> sift.vcf.gz

zcat "$SIFT_DIR"/MT.gz | cut -f 1,2,3,4,6,7,8,9,10,11 | grep -v "NA$" | grep -v "^#" | python3 create-a-vcf-body-from-sift-chromosome-file.py chrM | bgzip >> sift.vcf.gz

tabix -p vcf sift.vcf.gz
```

The resulting VCF file **sift.vcf.gz** has the following INFO fields:

 * **ISEQ_SIFT_MIN** - Minimal SIFT score for a variant
 * **ISEQ_SIFT** - SIFT score annotations: 'Transcript_Id | Gene_Name | Region | HGVS.p | SIFT_score

**List of SIFT INFO fields**

**Source:** SIFT4G
**Version:** SIFT4G, GRCh38.83

| INFO          | Number | Type   | Description                                                                        |
|---------------|--------|--------|------------------------------------------------------------------------------------|
| ISEQ_SIFT_MIN | 1      | Float  | Minimal SIFT score for a variant                                                   |
| ISEQ_SIFT     | .      | String | SIFT score annotations: 'Transcript_Id \| Gene_Name \| Region \| HGVS.p \| SIFT_score' |
