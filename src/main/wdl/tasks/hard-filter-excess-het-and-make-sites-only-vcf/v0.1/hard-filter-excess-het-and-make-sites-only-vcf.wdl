# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  Hard Filter And Make Sites Only Vcf
#  -----------------------------------
#    Version: v0.1
#    Status: Beta
#    Last edited: 06-05-2019
#    Last edited by: Katarzyna Kolanek
#    Author(s):
#      + Katarzyna Kolanek, <katarzyna.kolanek@intelliseq.pl>, https://gitlab.com/lltw
#    Maintainer(s):
#      + Katarzyna Kolanek <katarzyna.kolanek@intelliseq.pl>, https://gitlab.com/lltw
#    Copyright: Copyright 2019 Intelliseq
#    License: All rights reserved
#
#  Desription:
#  -----------
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow hard_filter_excess_het_and_make_sites_only_vcf {}

task hard_filter_excess_het_and_make_sites_only_vcf {

  File vcf_gz
  File vcf_gz_tbi
  Float excess_het_threshold = 54.69

  String vcf_basename

  String docker_image = "intelliseqngs/snpsift:0.1"
  String snpsift_java_mem = "-Xmx4g"
  String snpsift_jar = "/opt/tools/snpEff/SnpSift.jar"

  String task_name = "hard_filter_excess_het_and_make_sites_only_vcf"
  String task_ver = "v1.0"

  command <<<

    set -e

    zcat ${vcf_gz} \
    | java ${snpsift_java_mem} -jar ${snpsift_jar} filter --addFilter ExcessHet "ExcessHet > ${excess_het_threshold}" --filterId ExcessHet \
    | tee >(bgzip > ${vcf_basename}.excess-het-filtered.vcf.gz) \
    | awk '{if (/^#/) {if (!/^##FORMAT/) {print}} else {print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8}}' \
    | bgzip > ${vcf_basename}.excess-het-filtered.sites-only.vcf.gz

    tabix -p vcf ${vcf_basename}.excess-het-filtered.vcf.gz &
    tabix1=$!
    tabix -p vcf ${vcf_basename}.excess-het-filtered.sites-only.vcf.gz &
    tabix2=$!
    wait $tabix1 $tabix2


    JAVA_VER=`java -version 2>&1 | awk '/openjdk version/ {print $3}' | tr -d '"'`
    JAVA=`echo '"java_openjdk":"'$JAVA_VER',GPLv2"'`
    SNP_VER=`java -jar ${snpsift_jar} annotate -h 2>&1 | awk '/version/ {print $3}'`
    SNP=`echo '"snpsift":"'$SNP_VER',LGPLv3"'`
    TABIX_VER=`tabix 2>&1 | awk '/Version/ {print $2}'`
    TABIX=`echo '"tabix":"'$TABIX_VER',MIT"'`
    BGZIP_VER=`bgzip -h 2>&1 | awk '/Version/ {print $2}'`
    BGZIP=`echo '"bgzip":"'$BGZIP_VER',MIT"'`
    UBUNTU_VER=`awk '/PRETTY_NAME/ {print $2}' /etc/*release`
    UBUNTU=`echo '"ubuntu":"'$UBUNTU_VER',GPL"'`

    echo "\""${task_name}"\""":{\"task-version\":\""${task_ver}"\","$JAVA","$TABIX","$BGZIP","$SNP","$UBUNTU"}" > hard-filter-excess-het-and-make-sites-only-vcf.task-info.json

  >>>

  runtime {

    docker: docker_image

  }

  output {

    File excess_het_filtered_vcf_gz = "${vcf_basename}.excess-het-filtered.vcf.gz"
    File excess_het_filtered_vcf_gz_tbi = "${vcf_basename}.excess-het-filtered.vcf.gz.tbi"
    File excess_het_filtered_sites_only_vcf_gz = "${vcf_basename}.excess-het-filtered.sites-only.vcf.gz"
    File excess_het_filtered_sites_only_vcf_gz_tbi = "${vcf_basename}.excess-het-filtered.sites-only.vcf.gz.tbi"

    File hard_filter_excess_het_and_make_sites_only_vcf_log = stderr()
    File hard_filter_excess_het_and_make_sites_only_vcf_task_info = "hard-filter-excess-het-and-make-sites-only-vcf.task-info.json"

  }

}
