# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  Task name
#  ---------
#    Version: vx.x
#    Status: [Prototype | Beta | Production]
#    Last edited: [dd-mm-yy]
#    Last edited by: <name surname>
#    Author(s):
#      + <name surname> <e-mail adress> <Gitlab URL>,
#      (...)
#      + <name surname> <e-mail adress> <Gitlab URL>
#    Maintainer(s):
#      + <name surname> <e-mail adress> <Gitlab URL>,
#      (...)
#      + <name surname> <e-mail adress> <Gitlab URL>
#    Copyright: Copyright 2019 Intelliseq
#    Licence: All rights reserved
#    Copyright: Copyright 2019 Intelliseq
#    Licence: All rights reserved
#
#  Desription:
#  -----------
#    <Description of the task>
#
#  Changes from vy.y:  (OPTIONAL)
#  -------------------
#    <Changes introduced since the last version of the task>
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


workflow task_name_workflow {}

task task_name {

  # Task inputs

  #
  #
  #

  File vcf_gz
  File vcf_gz_tbi
  Float excess_het_threshold = 54.69

  String vcf_basename

#!#
#># Docker image (MANDATORY)
  #
  # String docker_image = "intelliseqngs/snpsift:v4.3t_v0.0.1"

  #
  #
    String snpsift_java_mem = "-Xmx4g"
    String snpsift_jar = "/opt/tools/snpEff/SnpSift.jar"

#!#
#># Task name and task version (MANDATORY)
  #

  String task_name = "<task-name>"
  String task_ver = "v.x.x"

  command <<<

    set -e

    # Task info

    JAVA_VER=`java -version 2>&1 | awk '/openjdk version/ {print $3}' | tr -d '"'`
    JAVA=`echo '"java_openjdk":"'$JAVA_VER',GPLv2"'`
    SNP_VER=`java -jar ${snpsift_jar} annotate -h 2>&1 | awk '/version/ {print $3}'`
    SNP=`echo '"snpsift":"'$SNP_VER',LGPLv3"'`
    TABIX_VER=`tabix 2>&1 | awk '/Version/ {print $2}'`
    TABIX=`echo '"tabix":"'$TABIX_VER',MIT"'`
    BGZIP_VER=`bgzip -h 2>&1 | awk '/Version/ {print $2}'`
    BGZIP=`echo '"bgzip":"'$BGZIP_VER',MIT"'`
    UBUNTU_VER=`awk '/PRETTY_NAME/ {print $2}' /etc/*release`
    UBUNTU=`echo '"ubuntu":"'$UBUNTU_VER',GPL"'`

    echo "\""${task_name}"\""":{\"task-version\":\""${task_ver}"\","$JAVA","$TABIX","$BGZIP","$SNP","$UBUNTU"}" > hard-filter-excess-het-and-make-sites-only-vcf.task-info.json

  >>>

  runtime {

    docker: docker_image

  }

  output {

    ###########
    # Outputs #
    ###########

    #  a) VCF files
    #
    #    + All output VCF files must be bgzipped and indexed by tabic -p vcf
    #
    #    + Naming convention:
    #
    #      File <what-was-done-to-vcf-1>_vcf_gz = "${vcf_basename}.<what-was-done-to-vcf-1>.vcf.gz"
    #      File <what-was-done-to-vcf-1>_vcf_gz_tbi = "${vcf_basename}.<what-was-done-to-vcf-1>.vcf.gz.tbi"
    #      File <what-was-done-to-vcf-1>_<what-was-done-to-vcf-2>_vcf_gz = "${vcf_basename}.<what-was-done-to-vcf-1>.<what-was-done-to-vcf-2>.vcf.gz"
    #      File <what-was-done-to-vcf-1>_<what-was-done-to-vcf-2>_vcf_gz_tbi = "${vcf_basename}.<what-was-done-to-vcf-1>.<what-was-done-to-vcf-2>.vcf.gz.tbi"
    #
    #    + Examples:
    #
    #      File excess_het_filter_added_vcf_gz = "${vcf_basename}.excess-het-filter-added.vcf.gz"
    #      File excess_het_filter_added_vcf_gz_tbi = "${vcf_basename}.excess-het-filter-added.vcf.gz.tbi"
    #      File excess_het_filter_added_sites_only_vcf_gz = "${vcf_basename}.excess-het-filter-added.sites-only.vcf.gz"
    #      File excess_het_filter_added_sites_only_vcf_gz_tbi = "${vcf_basename}.excess-het-filter-added.sites-only.vcf.gz.tbi"

    #######################
    #  Logs and task info #
    #######################

    #  a) Tool specific logs examples
    #
    #    + Tool specific logs should be created for tools that produce stderr and/or stdout
    #      with information regarding status/progress/statistics etc. Examples of such tools
    #      are HaplotypeCaller or ApplyBQSR.
    #
    #    + There is no need to output separate log files fot tools that produce stderr and/or
    #      stdout only in case of error.
    #
    #    + The contents of tool specific logs must also be included in generic logs (see below)
    #
    #    + Naming convention:
    #
    #      File <tool-1-name>_stderr_log = "<tool-name>.stderr.log"
    #      File <tool-2-name>_stdout_log = "<tool-name>.stdout.log"
    #      File <tool-3-name>_stderr_stdout_log = "<tool-name>.stderr.stdout.log"
# ! #
#-->#  b) Task generic logs (MANDATORY)
    #
    #    + Every task must output generic logs containing all stderr an stdout, including
    #      contents of tool specific logs
    #
    #    + Beware of pipes - capture stderr from piped tools in a way that separates stderr
    #      from different tools (to improve readability)
    #
    #    + Naming convention:

    File stderr_log = stderr()
    File stdout_log = stderr()

# ! #
#--># c) Task info JSON (MANDATORY)
    #
    #    + Naming convention:

    File task_info = "<task-name>.task-info.json"

  }

}
t
