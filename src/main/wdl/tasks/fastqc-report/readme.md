## task
fastqc-report

## description
Generates report by fastqc tool.

## changes
### v1.0 (05.04.2019)
Initial version of task.
