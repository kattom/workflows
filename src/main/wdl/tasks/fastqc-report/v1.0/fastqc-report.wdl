task generate_fastqc_report {

  File fastq
  String sample_id = "no_id_provided"
  String filename = basename(fastq, "fq.gz")

  String docker_image = "res10.softsystem.pl/ngs/ngs-prototype/fastqc:0.11.17"

  command <<<

    fastqc ${fastq_1} -o . &> ${sample_id}.${filename}_1.fastqc.stdout.stderr.log
  	fastqc ${fastq_2} -o . &> ${sample_id}.${filename}_2.fastqc.stdout.stderr.log

  	unzip ${filename}_1_fastqc.zip
  	unzip ${filename}_2_fastqc.zip

  	cat ${filename}_1_fastqc/summary.txt | awk -F '\t' 'BEGIN {print "statistic"} {print $2}' > statistic
  	cat ${filename}_1_fastqc/summary.txt | awk -F '\t' 'BEGIN {print "fastq_1-status"} {print $1}' > fastq_1-status
  	cat ${filename}_2_fastqc/summary.txt | awk -F '\t' 'BEGIN {print "fastq_2-status"} {print $1}' > fastq_2-status

  	paste -d '\t' statistic fastq_1-status fastq_2-status > ${sample_id}.${filename}.fastqc-statistics.txt

    cp ${filename}_1_fastqc/Images/per_base_quality.png ${sample_id}.${filename}_1.fastqc-report.first-image.png
    cp ${filename}_2_fastqc/Images/per_base_quality.png ${sample_id}.${filename}_2.fastqc-report.first-image.png

    cp ${filename}_1_fastqc/fastqc_data.txt ${sample_id}.${filename}_1.fastqc-data.txt
    cp ${filename}_2_fastqc/fastqc_data.txt ${sample_id}.${filename}_2.fastqc-data.txt

  	mv ${filename}_1_fastqc.html ${sample_id}.${filename}_1.fastqc-report.html
  	mv ${filename}_2_fastqc.html ${sample_id}.${filename}_2.fastqc-report.html

  	mv ${filename}_1_fastqc.zip ${sample_id}.${filename}_1.fastqc-report.zip
  	mv ${filename}_2_fastqc.zip ${sample_id}.${filename}_2.fastqc-report.zip

  	if grep -q "FAIL" ${sample_id}.${filename}.fastqc-statistics.txt; then
  	  echo "FAILED" > fastqc_status;
  	else
  	  echo "PASSED" > fastqc_status;
  	fi

  echo "TRUE" > qc_status

  >>>

  runtime {
    docker: docker_image
  }

  output {

    String qc_status = read_string("qc_status")

    File fastqc_1_report_html = "${sample_id}.${filename}_1.fastqc-report.html"
    File fastqc_2_report_html = "${sample_id}.${filename}_2.fastqc-report.html"

    File fastqc_1_report_zip = "${sample_id}.${filename}_1.fastqc-report.zip"
    File fastqc_2_report_zip = "${sample_id}.${filename}_2.fastqc-report.zip"

    File fastqc_statistics_txt = "${sample_id}.${filename}.fastqc-statistics.txt"

    File fastqc_1_report_data = "${sample_id}.${filename}_1.fastqc-data.txt"
    File fastqc_2_report_data = "${sample_id}.${filename}_2.fastqc-data.txt"

    File fastqc_1_report_image = "${sample_id}.${filename}_1.fastqc-report.first-image.png"
    File fastqc_2_report_image = "${sample_id}.${filename}_2.fastqc-report.first-image.png"

    File fastqc_1_stdout_stderr_log = "${sample_id}.${filename}_1.fastqc.stdout.stderr.log"
    File fastqc_2_stdout_stderr_log = "${sample_id}.${filename}_2.fastqc.stdout.stderr.log"

    String fastqc_status = read_string("fastqc_status")

  }
}

workflow wfl {
}
