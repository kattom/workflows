# @title hello
# @version 1
# @author Marcin Piechota
# @copyright Copyright 2019 Intelliseq
# @license All rights reserved
# @maintainer Marcin Piechota
# @email piechota [-@-] intelliseq.pl
# @status prototype 
# @edited 6.03.2019

task sayhello {
  String name = "default"

  command {
    echo 'Hello ${name}!'
  }
  output {
    File response = stdout()
  }
}

workflow hello {
  call sayhello
}