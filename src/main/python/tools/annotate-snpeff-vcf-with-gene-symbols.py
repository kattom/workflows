#!/usr/bin/env python3

import argparse
import re
from sys import stdin
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(description='Annotates a VCF file with symbols of genes, that a given variant have'
                                         ' specified or greater than specified impact on. Input VCF must be'
                                         ' previously annotated by SnpEff.')

parser.add_argument('--impact', type=str, default='MODERATE', choices={'HIGH', 'MODERATE', 'LOW', 'MODIFIER'},
                    help="IMPACT threshold")
parser.add_argument('--field-name', type=str, default='ISEQ_GENES_NAMES', help="Name of a field with genes symbols in output VCF file")
parser.add_argument('--version', type=str, default='4.3t', help="Version of SnpEff")

args = parser.parse_args()
threshold = args.impact
field = args.field_name
version = args.version

levels = {'HIGH': 0, 'MODERATE': 1, 'LOW': 2, 'MODIFIER': 3}

for line in stdin:
    if line[0:14] == "##INFO=<ID=ANN":
        info_field = re.sub('##INFO=<ID=ANN.*?\'', '', line)
        info_field = re.sub('\' ">', '', info_field)

        info_field_list = info_field.split(" | ")

        annotation_impact_index = [i for i, s in enumerate(info_field_list) if 'Annotation_Impact' in s][0]
        gene_name_index = [i for i, s in enumerate(info_field_list) if 'Gene_Name' in s][0]

        print(line.rstrip())
        print("##INFO=<ID=" + field + '",Number=.,Type=String,Description="Names of genes the variant has at least ' + threshold + ' impact on",Source="SnpEff",Version="' + version + '">''')
    elif line[0] == "#":
        print(line.rstrip())
    else:
        # create list
        line_list = line.split('\t')
        # extract INFO field
        info_list = line_list[7].split(';')
        # find ANNO field
        where_anno = [i for i, s in enumerate(info_list) if s.startswith("ANN=")]
        where_anno = where_anno[0]
        # extract ANNO filed
        annotations = info_list[where_anno].strip()
        annotations_list = re.sub('^ANN=', '', annotations).split(",")

        genes = []
        for anno in annotations_list:
            anno = anno.split('|')
            if levels[anno[annotation_impact_index]] <= levels[threshold]:
                genes.append(anno[gene_name_index])

        genes = ','.join(list(set(genes)))

        if genes == "":
            print(line.rstrip())
        else:
            info_list[where_anno] = annotations + ";" + field + "=" + genes
            info = ';'.join(info_list)
            line_list[7] = info
            line = '\t'.join(line_list)
            print(line.rstrip())
