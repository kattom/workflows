#!/usr/bin/env python3

from sys import stdin
import argparse
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)

parser = argparse.ArgumentParser(description='Annotate VCF file with a dictionary. VCF file must have an '
                                             'INFO field containing values of a dictionary. The field can contain multiple '
                                             'values. If a field contain multiple keys, the annotated field will contain '
                                             'comma delimited tuples KEY|VALUE.')

parser.add_argument('DICTIONARY_PATH', type=str, help="path to a dictionary")
parser.add_argument('KEY_COLUMN_NAME', type=str, help="specify name of a VCF INFO field containing keys")
parser.add_argument('FIELD_NAME', type=str, help="specify name of a field that will contain annotation")
parser.add_argument('HEADER', type=str, help="specify header line corresponding to a field containing annotation")
parser.add_argument('-k', type=str, default=',', help="specify delimiter in VCF INFO field containing keys (Default: ,)")
parser.add_argument('-d', type=str, default=',', help="specify delimiter in dictionary values (Default: ,)")
parser.add_argument('-t', type=str, default=',', help="specify delimiter delimiting tuples in a field containing annotation (Default: ,) ")
parser.add_argument('-i', type=str, default='|', help="specify delimiter in tuples delimiting KEY from VALUES (Default: |)")
parser.add_argument('-v', type=str, default=':', help="specify delimiter in tuples delimiting VALUES (Default: :)")

args = parser.parse_args()


dictionary_path = args.DICTIONARY_PATH
key_column_name = args.KEY_COLUMN_NAME
field_name = args.FIELD_NAME
header = args.HEADER
dictionary_delimiter = args.d
vcf_delimiter = args.k
new_field_delimiter = args.t
tuples_delimiter = args.i
tuples_values_delimeter = args.v

dictionary = {}

value_header = None
line_counter = 0

for line in open(dictionary_path, 'r'):

    if line.startswith('#') and line_counter == 0:
        line_counter += 1
    elif line.startswith('#') and line_counter == 1:
        value_header = line.strip()
        line_counter = 0
    else:

        record = [x.strip() for x in line.split('\t')]

        key = record[0]
        values = [x.strip() for x in record[1].split(dictionary_delimiter)]

        if key not in dictionary.keys():
            dictionary[key] = values
        else:
            dictionary[key] = dictionary[key] + values


for line in stdin:

    if line.startswith('##'):
        print(line.strip())

    elif line.startswith('#C'):

        print(header)
        print(line.strip())
    else:

        line = [x.strip() for x in line.split()]
        INFO = line[7].strip().split(';')

        new_annotation = []

        for info in INFO:

            if info.startswith(key_column_name):

                keys = info[len(key_column_name) + 1:].split(vcf_delimiter)

                for key in keys:

                    if key in dictionary.keys():

                         if dictionary[key] != ['.']:

                            new_annotation.append(key + tuples_delimiter + tuples_values_delimeter.join(dictionary[key]))

        if new_annotation:
            line[7] += ';' + field_name + '=' + new_field_delimiter.join(new_annotation)

        print('\t'.join(line))
