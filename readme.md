# Intelliseq workflows
Repository for wdl workflows for NGS analysis.

## Compiling
### Preparing python-tools release
```
scripts/python-prepare-release
```

### Build docker images
#### Symlinking scripts
```
sudo ln -s [path to project]/scripts/docker-build /usr/local/bin/docker-build
```
#### Building from dockerfile directory
```
docker-build --nochecksum --nopush
# OR
../../../../../scripts/docker-build --nochecksum --nopush
```

#### Building from project root directory
```
docker-build --nochecksum --nopush --target src/main/docker/[tool]/[version]
```

#### Build all docker images
```
docker login
scripts/docker-build --all
```

## Templates
[docker image template](docs/templates/docker-readme-template/readme.md)

## Rules for developers
1. Docker images are created in hierarchy 'ubuntu' -> 'ubuntu-toolbox' -> 'python-toolbox' -> 'java-toolbox' -> 'tool' or 'toolset'
2. Every docker directory has readme.md (search in templates)
3. Every docker directory has subdirectories with versions
4. 

## Other
[resources](docs/resources.md)
